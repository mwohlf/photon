package net.wohlfart.photon.input;

import org.lwjgl.util.vector.Vector3f;


public class TestInputEvents {
    
    public static MoveEvent createTestMoveEvent(float x, float y, float z) {
        return MoveEvent.move(x, y, z);
    }
    
    public static RotateEvent createTestRotateEvent(float deg, Vector3f axis) {
        return RotateEvent.rotate(deg, axis);
    }

}
