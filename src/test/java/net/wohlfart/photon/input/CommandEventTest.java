package net.wohlfart.photon.input;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Assert;
import org.junit.Test;

public class CommandEventTest {

    // there is no way of resetting a pool so a failing test might be a side effect of
    // a previous test leaving the pool empty

    @Test
    public void testPoolExaust() {
        CommandEvent[] events = new CommandEvent[CommandEvent.POOL_SIZE + 1];

        int c = 0;
        for (int i=CommandEvent.POOL_SIZE; i >= 0 ; --i ) {
            events[i] = CommandEvent.exit();
            c++;
        }

        // exausted pool returns null instead of throwing an excption
        assertEquals(CommandEvent.POOL_SIZE + 1, c);
        assertNull(events[0]);

        for (int i=CommandEvent.POOL_SIZE; i > 0 ; i-- ) {
            events[i].reset();
        }

    }

    @Test
    public void testPoolReturn() {
        CommandEvent[] events = new CommandEvent[CommandEvent.POOL_SIZE];

        int c = 0;
        for (int i=CommandEvent.POOL_SIZE -1 ; i >= 0 ; --i ) {
            events[i] = CommandEvent.exit();
            c++;
        }            
        assertNull(CommandEvent.exit()); // returns null

        assertEquals(CommandEvent.POOL_SIZE, c);

        assertNull(CommandEvent.exit());

        events[0].reset();

        events[0] = CommandEvent.exit();
        Assert.assertNotNull(events[0]);

        assertNull(CommandEvent.exit());

        // cleanup the pool
        for (int i=CommandEvent.POOL_SIZE - 1; i >= 0 ; i-- ) {
            events[i].reset();
        }

    }


    @Test
    public void testToMuchReturn() {
        CommandEvent extraEvent1;

        extraEvent1 = CommandEvent.exit();
        extraEvent1.reset();
        try {
            extraEvent1.reset();
            Assert.fail();
        } catch (IllegalStateException ex) {}

    }


}
