package net.wohlfart.photon.tools;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import net.wohlfart.photon.tools.ObjectPool.PoolableObject;

import org.junit.Assert;
import org.junit.Test;

public class EventBusTest {
    
    final List<PoolableEvent> eventCollector = new ArrayList<>();
    final List<PoolableEvent> resetCollector = new ArrayList<>();
    
    
    final static class NonPoolableEvent {
        NonPoolableEvent(int id) {
        }
    }

    final static class WrongListener {     
        @Subscribe
        public void listening(NonPoolableEvent evt) {
            Assert.fail();
        }        
    }

    
    final class PoolableEvent implements PoolableObject {
        PoolableEvent() {
        }
        @Override
        public void reset() {
            resetCollector.add(this);
        }
    }
     
    final class Listener {     
        @Subscribe
        public void listening(PoolableEvent evt) {
            eventCollector.add(evt);
        }        
    }

    
    @Test(expected=IllegalArgumentException.class)
    public void registerNonPoolable() {       
        EventBus<PoolableObject> bus = new SimpleEventBus();
        WrongListener listener = new WrongListener();       
        bus.register(listener);               
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void doubleRegister() {       
        EventBus<?> bus = new SimpleEventBus();        
        Listener listener1 = new Listener();       
        bus.register(listener1);               
        bus.register(listener1);               
    }
    
    @Test
    public void fireAndReset() {       
        EventBus<PoolableObject> bus = new SimpleEventBus();        
        Listener listener1 = new Listener();       
        bus.register(listener1); 
        
        bus.post(new PoolableEvent());
        assertEquals(0, eventCollector.size());
        assertEquals(0, resetCollector.size());
        
        bus.fireEvent();
        assertEquals(1, eventCollector.size());
        assertEquals(1, resetCollector.size());     
    }

        
}
