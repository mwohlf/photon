package net.wohlfart.photon.renderer;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EnumWeightsTest {

    @Test
    public void simpleCompare1() {
        EnumWeights weights = new EnumWeights(A.class);
        
        assertEquals(0, weights.getWeightFor(A.ZERO));
        assertEquals(1, weights.getWeightFor(A.ONE));
        assertEquals(2, weights.getWeightFor(A.TWO));
        assertEquals(3, weights.getWeightFor(A.THREE));
    }
   
    
    @Test
    public void simpleCompare2() {
        // enums like digits in a number:
        EnumWeights weights = new EnumWeights(A.class, B.class);
        
        assertEquals(0, weights.getWeightFor(A.ZERO));
        assertEquals(4, weights.getWeightFor(A.ONE));
        assertEquals(8, weights.getWeightFor(A.TWO));
        assertEquals(12, weights.getWeightFor(A.THREE));
        // a value must never be null the first enum is always the default
        assertEquals(12, weights.getWeightFor(A.THREE, B.ZERO));
        assertEquals(13, weights.getWeightFor(A.THREE, B.ONE));
        assertEquals(14, weights.getWeightFor(A.THREE, B.TWO));
        assertEquals(15, weights.getWeightFor(A.THREE, B.THREE));
    }
    
    
    
    enum A {
        ZERO, ONE, TWO, THREE;  
    }
    
    enum B {
        ZERO, ONE, TWO, THREE;  
    }
    
    
    
    
}
