package net.wohlfart.photon.renderer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import net.wohlfart.photon.renderer.LwjglRenderConfig.Blending;
import net.wohlfart.photon.renderer.LwjglRenderConfig.ClearColor;
import net.wohlfart.photon.renderer.LwjglRenderConfig.ClearDepth;
import net.wohlfart.photon.renderer.LwjglRenderConfig.ColorMask;
import net.wohlfart.photon.renderer.LwjglRenderConfig.DepthTest;
import net.wohlfart.photon.renderer.LwjglRenderConfig.FaceCulling;
import net.wohlfart.photon.renderer.LwjglRenderConfig.PrimitiveRestartIndex;
import net.wohlfart.photon.renderer.LwjglRenderConfig.SissorTest;
import net.wohlfart.photon.renderer.LwjglRenderConfig.StencilTest;

import org.junit.Test;

public class LwjglRenderStateTest {
        
    @Test
    public void simpleCompare() {
        
        LwjglRenderConfig a = new LwjglRenderConfig(
                Blending.OFF,
                ClearColor.GREY,
                ClearDepth.ONE,
                ColorMask.ON,
                DepthTest.OFF,
                FaceCulling.OFF,
                PrimitiveRestartIndex.MAX_INT,
                SissorTest.OFF,
                StencilTest.OFF);
        
        LwjglRenderConfig b = new LwjglRenderConfig(
                Blending.OFF,
                ClearColor.GREY,
                ClearDepth.ONE,
                ColorMask.ON,
                DepthTest.OFF,
                FaceCulling.OFF,
                PrimitiveRestartIndex.MAX_INT,
                SissorTest.OFF,
                StencilTest.OFF);
        
        assertEquals(a.hashCode(), b.hashCode());
        
        
        LwjglRenderConfig c = new LwjglRenderConfig(
                Blending.ON,
                ClearColor.GREY,
                ClearDepth.ONE,
                ColorMask.ON,
                DepthTest.OFF,
                FaceCulling.OFF,
                PrimitiveRestartIndex.MAX_INT,
                SissorTest.OFF,
                StencilTest.OFF);
        
        assertEquals(a,b);
        assertTrue(" a: " + a.hashCode() + "  c: " + c.hashCode(),  a.hashCode() < c.hashCode());         

    }

}
