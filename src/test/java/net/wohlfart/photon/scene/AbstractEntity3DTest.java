package net.wohlfart.photon.scene;

import static org.junit.Assert.assertEquals;

import javax.vecmath.Vector3d;

import net.wohlfart.photon.input.MoveEvent;
import net.wohlfart.photon.input.RotateEvent;
import net.wohlfart.photon.input.TestInputEvents;
import net.wohlfart.photon.scene.graph.SceneGraph;
import net.wohlfart.photon.tools.MathTool;

import org.junit.Before;
import org.junit.Test;
import org.lwjgl.util.vector.Quaternion;

public class AbstractEntity3DTest {
       
    SceneImpl unitUnderTest;
    AbstractEntity3D entity;
    
    @Before
    public void before() {    
        entity = new Entity3DImpl();
        SceneGraph sceneGraph = new SceneGraph();
        sceneGraph.addEntity(entity);
        unitUnderTest = new SceneImpl();
        unitUnderTest.setSceneGraph(sceneGraph);     
    }
       
    @Test
    public void testSimpleMove() {
        MoveEvent mov; // using right hand coordinate system...
        
        // origin is moving one unit forward...
        mov = TestInputEvents.createTestMoveEvent(0, 0, -1);       
        unitUnderTest.move(mov);   
        unitUnderTest.update(0);        
        // entity should stay at its position, means it moved one unit towards the origin
        assertEquals(new Vector3d(0,0,1), entity.getPosition());
             
        // origin moving to the right
        mov = TestInputEvents.createTestMoveEvent(1, 0, 0);       
        unitUnderTest.move(mov);   
        unitUnderTest.update(0);
        // entity should stay at its position, means it moved one unit towards the left
        assertEquals(new Vector3d(-1,0,1), entity.getPosition());
        
        // origin moving up
        mov = TestInputEvents.createTestMoveEvent(0, 2, 0);       
        unitUnderTest.move(mov);   
        unitUnderTest.update(0);
        // entity should stay at its position, means it moved one unit towards the left
        assertEquals(new Vector3d(-1,-2,1), entity.getPosition());       
    }
    
    
    @Test
    public void testSimpleRotate() {
        RotateEvent rot; // using right hand coordinate system...
        Quaternion q = new Quaternion();
        
        // put entity just in front
        this.entity.position.x = 0;
        this.entity.position.y = 0;
        this.entity.position.z = -1;

        assertEqualsVec(new Vector3d(0,0,-1), entity.getPosition());       
        assertEqualsQuat(q, entity.getRotation());       

        
        rot = TestInputEvents.createTestRotateEvent(MathTool.HALF_PI, MathTool.Y_AXIS);         
        // origin rotating 90 degree to the left
        unitUnderTest.rotate(rot);   
        unitUnderTest.update(0);
        // entity should now be to the left
        assertEqualsVec(new Vector3d(-1,0,0), entity.getPosition());
        MathTool.rotate(q, MathTool.HALF_PI, MathTool.Y_AXIS);
        assertEqualsQuat(q, entity.getRotation());       

        // rotate back
        rot = TestInputEvents.createTestRotateEvent(-MathTool.HALF_PI, MathTool.Y_AXIS);       
        unitUnderTest.rotate(rot);   
        unitUnderTest.update(0);
        assertEqualsVec(new Vector3d(0,0,-1), entity.getPosition());       
        MathTool.rotate(q, -MathTool.HALF_PI, MathTool.Y_AXIS);
        assertEqualsQuat(q, entity.getRotation());       

        // rotate 180 degree
        rot = TestInputEvents.createTestRotateEvent(-MathTool.PI, MathTool.Y_AXIS);       
        unitUnderTest.rotate(rot);   
        unitUnderTest.update(0);
        assertEqualsVec(new Vector3d(0,0,1), entity.getPosition());   
        MathTool.rotate(q,-MathTool.PI, MathTool.Y_AXIS);
        assertEqualsQuat(q, entity.getRotation());       

        
        // rot around z axis shouldn't change the position
        rot = TestInputEvents.createTestRotateEvent(-MathTool.HALF_PI, MathTool.Z_AXIS);       
        unitUnderTest.rotate(rot);   
        unitUnderTest.update(0);
        assertEqualsVec(new Vector3d(0,0,1), entity.getPosition()); 
        MathTool.rotate(q,-MathTool.HALF_PI, MathTool.Z_AXIS);
        assertEqualsQuat(q, entity.getRotation());       

       
        // rot around x axis moves up
        rot = TestInputEvents.createTestRotateEvent(-MathTool.HALF_PI, MathTool.X_AXIS);       
        unitUnderTest.rotate(rot);   
        unitUnderTest.update(0);
        assertEqualsVec(new Vector3d(0,1,0), entity.getPosition());      
        MathTool.rotate(q,-MathTool.HALF_PI, MathTool.X_AXIS);
        assertEqualsQuat(q, entity.getRotation());       
        
    }

    private void assertEqualsQuat(Quaternion expected, Quaternion actual) {
        assertEquals(expected.x, actual.x, 0.000001);
        assertEquals(expected.y, actual.y, 0.000001);
        assertEquals(expected.z, actual.z, 0.000001);  
        assertEquals(expected.w, actual.w, 0.000001);  
    }

    private void assertEqualsVec(Vector3d expected, Vector3d actual) {
        assertEquals(expected.x, actual.x, 0.000001);
        assertEquals(expected.y, actual.y, 0.000001);
        assertEquals(expected.z, actual.z, 0.000001);
    }  
    
}
