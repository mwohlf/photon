package net.wohlfart.photon.scene;

import java.util.Collection;
import java.util.HashSet;

import net.wohlfart.photon.scene.graph.AbstractRenderElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * simple example how to implement a Entity3D
 */
public class Entity3DImpl extends AbstractEntity3D {
    protected static final Logger LOGGER = LoggerFactory.getLogger(Entity3DImpl.class);

    protected final Collection<AbstractRenderElement> renderElements = new HashSet<>();

  
    
    @Override
    public Collection<AbstractRenderElement> getRenderCommands() {
        return renderElements;
    }

    @Override
    public void setup() {
        // nothing to do
    }

    @Override
    public void destroy() {
        // nothing to do
    }

    static class RenderCommand extends AbstractRenderElement {
        // nothing to do
    }

};
