package net.wohlfart.photon.scene.graph.elem;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import javax.vecmath.Vector3d;

import org.junit.Test;

public class BoundingSphereTest {
    
    private static final Vector3d ORIGIN = new Vector3d(0,0,0);
    private static final Vector3d A = new Vector3d( 5, 0, 0);
    private static final Vector3d B = new Vector3d(-5, 0, 0);
    private static final Vector3d C = new Vector3d( 0, 0, 5);
    private static final Vector3d D = new Vector3d( 0, 0,-5);
    private static final Vector3d E = new Vector3d( 0, 5, 0);
    private static final Vector3d F = new Vector3d( 0,-5, 0);

    @Test
    public void testLineIntersection() {  
        
        BoundingVolumeSphere sphere = new BoundingVolumeSphere(ORIGIN, 1);
        
        assertTrue(sphere.intersects(A, B));
        assertTrue(sphere.intersects(C, D));
        assertTrue(sphere.intersects(E, F));
               
        assertFalse(sphere.intersects(new Vector3d( 1, 5, 0), new Vector3d( 1, -5, 0)));
        assertTrue(sphere.intersects(new Vector3d( 0.9, 5, 0), new Vector3d( 1, -5, 0)));       
    }
    
    
    @Test
    public void testSphereIntersection() {  
        
        BoundingVolumeSphere sphere1 = new BoundingVolumeSphere(A, 5);
        BoundingVolumeSphere sphere2 = new BoundingVolumeSphere(B, 5);
        BoundingVolumeSphere sphere3 = new BoundingVolumeSphere(B, 5.25);
        BoundingVolumeSphere sphere4 = new BoundingVolumeSphere(A, 4.75);
        
        assertTrue(sphere1.intersects(sphere1));
        assertFalse(sphere1.intersects(sphere2));
        assertTrue(sphere1.intersects(sphere3));
        assertTrue(sphere1.intersects(sphere4));
   
        assertFalse(sphere2.intersects(sphere1));
        assertTrue(sphere2.intersects(sphere2));
        assertTrue(sphere2.intersects(sphere3));
        assertFalse(sphere2.intersects(sphere4));
        
        assertTrue(sphere3.intersects(sphere1));
        assertTrue(sphere3.intersects(sphere2));
        assertTrue(sphere3.intersects(sphere3));
        assertFalse(sphere3.intersects(sphere4));
 
        assertTrue(sphere4.intersects(sphere1));
        assertFalse(sphere4.intersects(sphere2));
        assertFalse(sphere4.intersects(sphere3));
        assertTrue(sphere4.intersects(sphere4));    
        
    }

    @Test
    public void testContainsFully() {  
        
        BoundingVolumeSphere sphere1 = new BoundingVolumeSphere(A, 5);
        BoundingVolumeSphere sphere2 = new BoundingVolumeSphere(B, 5);
        BoundingVolumeSphere sphere3 = new BoundingVolumeSphere(B, 5.25);
        BoundingVolumeSphere sphere4 = new BoundingVolumeSphere(A, 4.75);
        
        assertTrue(sphere1.containsFully(sphere1));
        assertFalse(sphere1.containsFully(sphere2));
        assertFalse(sphere1.containsFully(sphere3));
        assertTrue(sphere1.containsFully(sphere4));
   
        assertFalse(sphere2.containsFully(sphere1));
        assertTrue(sphere2.containsFully(sphere2));
        assertFalse(sphere2.containsFully(sphere3));
        assertFalse(sphere2.containsFully(sphere4));
        
        assertFalse(sphere3.containsFully(sphere1));
        assertTrue(sphere3.containsFully(sphere2));
        assertTrue(sphere3.containsFully(sphere3));
        assertFalse(sphere3.containsFully(sphere4));
 
        assertFalse(sphere4.containsFully(sphere1));
        assertFalse(sphere4.containsFully(sphere2));
        assertFalse(sphere4.containsFully(sphere3));
        assertTrue(sphere4.containsFully(sphere4));    
        
    }

    @Test
    public void testMerge() {
        BoundingVolumeSphere result;
        BoundingVolumeSphere sphere1 = new BoundingVolumeSphere(A, 5);
        BoundingVolumeSphere sphere2 = new BoundingVolumeSphere(B, 5);
        BoundingVolumeSphere sphere3 = new BoundingVolumeSphere(B, 5.25);
        BoundingVolumeSphere sphere4 = new BoundingVolumeSphere(A, 4.75);

        result = sphere1.merge(sphere2);
        assertEquals(0d, result.center.x, 0.001);
        assertEquals(0d, result.center.y, 0.001);
        assertEquals(0d, result.center.z, 0.001);
        assertEquals(20d, result.radius, 0.001);

        result = sphere1.merge(sphere3);
        assertEquals(-0.125d, result.center.x, 0.001);
        assertEquals(0d, result.center.y, 0.001);
        assertEquals(0d, result.center.z, 0.001);
        assertEquals(20.25d, result.radius, 0.001);

        result = sphere1.merge(sphere4);
        assertEquals(5d, result.center.x, 0.001);
        assertEquals(0d, result.center.y, 0.001);
        assertEquals(0d, result.center.z, 0.001);
        assertEquals(5d, result.radius, 0.001);           
    }
   
}
