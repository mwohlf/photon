package net.wohlfart.photon.scene.graph;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Iterator;

import org.junit.Test;

public class TreeImplTest {

    @Test
    public void noParent() {
        try {
            new TreeImpl<String>(null, "root", new ArrayList<TreeImpl<String>>());
        } catch (java.lang.Exception | java.lang.AssertionError ex) {
            // expected
        }
    }

    @Test
    public void testOrdering() {      
        TreeImpl<String> tree = new TreeImpl<String>("root");

        tree.add("A");  
        TreeImpl<String> b = tree.add("B");
        tree.add("C");
        tree.add("D");            
        b.add("BB");

        Iterator<String> preOrder = tree.createPreOrderTreeIterator();       
        assertEquals("root", preOrder.next());
        assertEquals("A", preOrder.next());
        assertEquals("B", preOrder.next());
        assertEquals("BB", preOrder.next());
        assertEquals("C", preOrder.next());
        assertEquals("D", preOrder.next());
    }
    
    
    
    @Test
    public void testRemove() {      
        TreeImpl<String> tree = new TreeImpl<String>("root");

        tree.add("A");  
        TreeImpl<String> b = tree.add("B");
        tree.add("C");
        tree.add("D");            
        b.add("BB");

        Iterator<String> preOrder = tree.createPreOrderTreeIterator();       
        assertEquals("root", preOrder.next());
        assertEquals("A", preOrder.next());
        assertEquals("B", preOrder.next());
        assertEquals("BB", preOrder.next());
        assertEquals("C", preOrder.next());
        assertEquals("D", preOrder.next());
    }


}
