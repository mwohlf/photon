#version 330 core

in vec4 pass_Color;

// see: http://stackoverflow.com/questions/8632550/how-does-the-default-glsl-shaders-look-like-for-version-330

out vec4 fragColor;
 
void main(void) {
    fragColor = pass_Color;
}
