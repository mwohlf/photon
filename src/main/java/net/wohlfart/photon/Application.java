package net.wohlfart.photon;

import net.wohlfart.photon.input.CommandEvent;
import net.wohlfart.photon.input.InputLoop;
import net.wohlfart.photon.network.NetworkLoop;
import net.wohlfart.photon.state.DefaultState;
import net.wohlfart.photon.state.State;
import net.wohlfart.photon.state.StateManager;
import net.wohlfart.photon.time.Clock;
import net.wohlfart.photon.time.Timer;
import net.wohlfart.photon.time.TimerImpl;
import net.wohlfart.photon.tools.Subscribe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * this class fires up the applications tasks and waits for them to complete
 */
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    private RenderLoop renderLoop;
    private InputLoop inputLoop;
    private NetworkLoop networkLoop;
    private boolean useThreads;

    private volatile boolean isTerminated;

    private Clock clock;

    private StateManager stateManager;

    public void setUseThreads(boolean useThreads) {
        this.useThreads = useThreads;
    }

    public void setClock(Clock clock) {
        this.clock = clock;
    }

    public void setStateManager(StateManager stateManager) {
        this.stateManager = stateManager;
    }

    public void setRenderLoop(RenderLoop renderLoop) {
        this.renderLoop = renderLoop;
    }

    public void setInputLoop(InputLoop inputLoop) {
        this.inputLoop = inputLoop;
    }

    public void setNetworkLoop(NetworkLoop networkLoop) {
        this.networkLoop = networkLoop;
    }
    

    public void start() throws GenericException {
        if (useThreads) {
            LOGGER.info("starting threads now...");

            renderLoop.start();
            inputLoop.start();
            networkLoop.start();
            LOGGER.info("...joining threads...");

            renderLoop.join();
            inputLoop.join();
            networkLoop.join();
            LOGGER.info("...finished"); 
        }
        else {
            LOGGER.info("starting main loop");

            renderLoop.startupPlatform();
            inputLoop.startupPlatform();
            networkLoop.startupPlatform();

            final Timer globalTimer = new TimerImpl(clock);

            State nextState = DefaultState.INIT_STATE;
            while (nextState != DefaultState.END_STATE && isTerminated == false) {
                renderLoop. switchToState(nextState);
                while (!nextState.isDone() && !isTerminated) {
                    float delta = globalTimer.getDelta();

                    inputLoop.processEvents(delta);
                    networkLoop.processEvents(delta);
                    renderLoop.updateAndRenderState(delta);                       

                }
                nextState = stateManager.calculateNextState(nextState);
            }

            globalTimer.dispose();
        }
    }

    @Subscribe
    public void terminateApplication(CommandEvent event) {
        LOGGER.debug("incoming command: " + event);
        switch (event.getKey()) {
            case EXIT:
                isTerminated = true;
                break;
            default:
                LOGGER.debug("unknown command: " + event + ", ignoring");
        }
    }

}
