package net.wohlfart.photon;

/**
 * Exception used for setup problems that might happen when booting up or
 * configuring the engine.
 */
public class GenericException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public GenericException(String message, Throwable cause) {
        super(message, cause);
    }

    public GenericException(String message) {
        super(message);
    }

}
