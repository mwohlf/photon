package net.wohlfart.photon.tools;

import java.io.Serializable;

public final class Dimension implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private int width;
    private int height;

    public Dimension(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public void setSize(int w, int h) {
        this.width = w;
        this.height = h;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

}
