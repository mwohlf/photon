package net.wohlfart.photon.scene;

import net.wohlfart.photon.ContextHolder;
import net.wohlfart.photon.Settings;
import net.wohlfart.photon.camera.CanMoveImpl;
import net.wohlfart.photon.camera.CanRotateImpl;
import net.wohlfart.photon.hud.SimpleLayer;
import net.wohlfart.photon.input.CommandEvent;
import net.wohlfart.photon.input.CommandEvent.CommandKey;
import net.wohlfart.photon.input.MoveEvent;
import net.wohlfart.photon.input.PointEvent;
import net.wohlfart.photon.input.RotateEvent;
import net.wohlfart.photon.picking.PickingRay;
import net.wohlfart.photon.picking.PickingRayBuilder;
import net.wohlfart.photon.renderer.Renderer;
import net.wohlfart.photon.scene.entity.ArrowSet;
import net.wohlfart.photon.scene.entity.ArrowSet.Arrow;
import net.wohlfart.photon.scene.entity.Corona;
import net.wohlfart.photon.scene.entity.Dust;
import net.wohlfart.photon.scene.entity.Earth;
import net.wohlfart.photon.scene.entity.ParticleEmitter;
import net.wohlfart.photon.scene.entity.ProceduralCelestial;
import net.wohlfart.photon.scene.entity.SimpleEffect;
import net.wohlfart.photon.scene.entity.Skybox;
import net.wohlfart.photon.scene.graph.ISceneGraph.IEntity3D;
import net.wohlfart.photon.scene.graph.SceneGraph;
import net.wohlfart.photon.state.DefaultState;
import net.wohlfart.photon.texture.CelestialType;
import net.wohlfart.photon.tools.Dimension;
import net.wohlfart.photon.tools.MathTool;
import net.wohlfart.photon.tools.PerspectiveProjectionFab;
import net.wohlfart.photon.tools.Subscribe;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

/**
 * central class contains anything that is rendered
 */
public class SceneImpl extends DefaultState {
    protected static final Logger LOGGER = LoggerFactory.getLogger(SceneImpl.class);

    protected Renderer renderer;

    protected SceneGraph sceneGraph;

    protected ArrowSet arrows = new ArrowSet();                 
    protected ParticleEmitter emitter = new ParticleEmitter();
    

    // delegates, the scene is moved not the cam
    private final CanRotateImpl rotation = new CanRotateImpl();
    private final CanMoveImpl movement = new CanMoveImpl();

    private volatile boolean debugOnce = false;


    @Override
    public void setup() {
        setupGraph();
    }

    public void setupGraph() { 
      
        new Skybox() .register(sceneGraph);
        
        new Dust() .register(sceneGraph);
        
        new SimpleLayer(renderer.getScreenDimension()) .register(sceneGraph);

             
        new ProceduralCelestial().withType(CelestialType.GAS_PLANET).withSize(10).withPosition( -100, 40, -60d) .register(sceneGraph);

        new ProceduralCelestial().withType(CelestialType.GREEN).withSize(40).withPosition( 100, -100, -60d) .register(sceneGraph);

        new ProceduralCelestial().withType(CelestialType.WATER_PLANET).withSize(8).withPosition( 300, -20, -60d) .register(sceneGraph);      

        new ProceduralCelestial()
            .withType(CelestialType.SUN)
            .withSize(30)
            .withPosition( 200, 30, -60d) 
            .withCorona(new Corona().withThinkness(1)) 
            .register(sceneGraph);
        
      
        arrows .register(sceneGraph);
        
        emitter .register(sceneGraph);
               
        new Earth()
            .withSize(5)
            .withPosition( 0, 0, -30d)
            .withCorona(new Corona().withThinkness(1)) 
            .register(sceneGraph);
             
        /*
        SimpleEffect effect = new SimpleEffect();
        effect.register(sceneGraph);        

        effect.addEntity(new ProceduralCelestial().withType(CelestialType.GAS_PLANET).withSize(10).withPosition( 0, 0, -90d));
        effect.addEntity(new Earth().withSize(5).withPosition( 0, 0, -30d));
        effect.addEntity(new ProceduralCelestial().withType(CelestialType.LAVA_PLANET).withSize(32).withPosition( 50, 50, -60d));
        */
            
    }

    @Required
    public void setRenderer(Renderer renderer) {
        this.renderer = renderer;
    }

    @Required
    public void setSceneGraph(SceneGraph sceneGraphs) {
        this.sceneGraph = sceneGraphs;
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public void update(float delta) {
        for (IEntity3D entity : sceneGraph.getEntities()) {
            entity.update(rotation.getRotation(), movement.getNegPosition(), delta);
        }
        movement.reset();
        rotation.reset();
    }

    @Override
    public void render() {   
        if (debugOnce) {
            renderer.setDebugMode(true);
            renderer.renderParent(sceneGraph.getRenderCache());            
            renderer.setDebugMode(false);
            debugOnce = false;
        } else {
            renderer.renderParent(sceneGraph.getRenderCache());            
        }
    }

    @Override
    public void dispose() {
        // TODO
    }

    @Subscribe 
    public void commandEvent(CommandEvent evt) { 
        LOGGER.debug("incoming command event: " + evt);
        if (evt.getKey() == CommandKey.DEBUG_RENDER) {
            debugOnce = true;
        }
    }

 
    @Subscribe
    public void pick(PointEvent pointEvent) {       
        LOGGER.info("pointEvent: " + pointEvent);
        
        if (arrows.sceneGraph == null) {
            return;
        }
          
        Dimension dim = renderer.getScreenDimension();
        
        Settings settings = ContextHolder.getSettings();
        final Matrix4f projectionMatrix = PerspectiveProjectionFab.create(settings);
  
        Vector3f posVector = new Vector3f();
        Matrix4f posMatrix = new Matrix4f();
        MathTool.convert(posVector, posMatrix);
        Matrix4f rotMatrix = new Matrix4f();
        MathTool.convert(new Quaternion(), rotMatrix);
        Matrix4f rotPosMatrix = new Matrix4f();
        Matrix4f.mul(rotMatrix, posMatrix, rotPosMatrix);

        Matrix4f modelViewMatrix = rotPosMatrix;
        
        PickingRay ray = new PickingRayBuilder()
        .setX(pointEvent.getX())
        .setY(pointEvent.getY())
        .setWidth(dim.getWidth())
        .setHeight(dim.getHeight())
        .setModelViewMatrix(modelViewMatrix)
        .setProjectionMatrix(projectionMatrix)
        .build();

        arrows.addArrow(new Arrow(ray.getStart(), ray.getEnd()));                    
    }

    @Subscribe 
    public void move(MoveEvent evt) {
        LOGGER.debug("incoming move event: " + evt);
        // adding move, since the cam is always at 0/0/0 and in -z this works 
        Vector3f.add(evt, movement.getPosition(), movement.getPosition());
    }

    @Subscribe 
    public void rotate(RotateEvent evt) { 
        LOGGER.debug("incoming rotate event: " + evt);
        // multiplying a quaternion means adding the rotations
        Quaternion.mul(evt, rotation.getRotation(), rotation.getRotation()); 
    }

}
