package net.wohlfart.photon.scene.graph;

import java.util.Iterator;

public interface Tree<T> {

    T getValue();

    Tree<T> add(T value);

    void remove(T value);
    
    Tree<T> firstMatch(Matcher<T> matcher);
    
    Iterator<? extends Tree<T>> getChildren();

}
