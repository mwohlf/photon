package net.wohlfart.photon.scene.graph;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class HashCollections<K, V> {
    
    private HashMap<K, Collection<V>> delegate = new HashMap<K, Collection<V>>();

    public static <K, V>  HashCollections<K, V> create() {
        return new HashCollections<K, V>();
    }

    public void putAll(K key, Iterable<? extends V> iter) {
        HashSet<V> collection = new HashSet<V>();     
        for (V elem : iter) {
            collection.add(elem);
        }
        delegate.put(key, collection);
    }

    public void removeAll(K key) {
        delegate.remove(key);
    }

    public Set<K> keySet() {
        return delegate.keySet();
    }

    public Collection<V> get(K key) {
        return delegate.get(key);
    }

}
