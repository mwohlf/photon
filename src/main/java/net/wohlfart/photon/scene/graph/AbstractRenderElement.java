package net.wohlfart.photon.scene.graph;

import java.util.HashMap;
import java.util.Map;

import net.wohlfart.photon.renderer.LwjglRenderConfig;
import net.wohlfart.photon.renderer.Renderer;
import net.wohlfart.photon.renderer.Renderer.IRenderElem;
import net.wohlfart.photon.renderer.Renderer.IRenderNode;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.graph.NodeSortStrategy.ISortToken;
import net.wohlfart.photon.shader.IShaderProgram.IShaderProgramIdentifier;
import net.wohlfart.photon.shader.ShaderIdentifier;
import net.wohlfart.photon.shader.ShaderTemplateWrapper;
import net.wohlfart.photon.shader.ShaderUniformHandle.ShaderUniformMatrixValue;
import net.wohlfart.photon.shader.ShaderUniformHandle.ShaderUniformValue;
import net.wohlfart.photon.texture.CelestialType;
import net.wohlfart.photon.texture.ITexture;
import net.wohlfart.photon.texture.ITexture.ITextureIdentifier;
import net.wohlfart.photon.texture.TextureIdentifier;

import org.lwjgl.util.vector.Matrix4f;


/**
 * this is the base component for rendering 3d objects
 * 
 * 
 * @author Michael Wohlfart
 */
public abstract class AbstractRenderElement implements IRenderElem {  
    
    protected final ShaderIdentifier SKYBOX_SHADER_ID = ShaderIdentifier
            .create("shader/skybox/vertex.glsl", "shader/skybox/fragment.glsl");
   
    protected final ShaderIdentifier TEXTURE_SIMPLE_SHADER_ID = ShaderIdentifier
            .create("shader/texture/simple.vert", "shader/texture/simple.frag");
   
    protected final ShaderIdentifier TEXTURE_RADBLUR_SHADER_ID = ShaderIdentifier
            .create("shader/texture/radblur.vert", "shader/texture/radblur.frag");

    protected final ShaderIdentifier SIMPLE_SHADER_ID = ShaderIdentifier
            .create("shader/simple/vertex.glsl", "shader/simple/fragment.glsl");

    // used for the labels, no z-coordinates
    protected final ShaderIdentifier PLAIN_SHADER_ID = ShaderIdentifier
            .create("shader/plain/vertex.glsl", "shader/plain/fragment.glsl");
    
    protected final ShaderIdentifier TWOD_SHADER_ID = ShaderIdentifier
            .create("shader/2d/vertex.glsl", "shader/2d/fragment.glsl");
  
    
 
    protected final ITextureIdentifier TEXTURE_ID1 = TextureIdentifier
            .create("gfx/textures/texture-1.jpg");

    protected final TextureIdentifier TEXTURE_ID2 =  TextureIdentifier
            .create(30, CelestialType.LAVA_PLANET, 2);

    protected final TextureIdentifier TEXTURE_ID3 =  TextureIdentifier
            .create(30, CelestialType.CONTINENTAL_PLANET, 2);
    
    

    protected LwjglRenderConfig renderConfig = LwjglRenderConfig.DEFAULT;
    
    protected IShaderProgramIdentifier shaderId = SIMPLE_SHADER_ID;
    
    protected ShaderUniformMatrixValue model2WorldValue =  new ShaderUniformMatrixValue(new Matrix4f());
    
    protected final Map<String, ShaderUniformValue> uniforms = new HashMap<>();
    
    protected final Map<String, ITexture> textures = new HashMap<>();
    
    protected double zOrder = Double.NaN;
    
    protected IGeometry geometry;
    
    protected ISortToken sortToken = new SortToken();
        

    protected AbstractRenderElement() {
        uniforms.put(ShaderTemplateWrapper.UNIFORM_MODEL_2_WORLD_MTX, model2WorldValue);
    }
    
    @Override
    public void setZOrder(double zOrder) {
        sortToken.setZOrder(zOrder);
    }
    
    @Override
    public Matrix4f getModel2WorldMatrix() {        
        return model2WorldValue.get();
    }
 

    @Override
    public final Map<String, ITexture> getTextures() {
        return textures;
    }

    @Override
    public IGeometry getGeometry() {
        return geometry;
    }

    @Override
    public void accept(Renderer renderer, Tree<IRenderNode> tree) {
        renderer.setConfig(shaderId, renderConfig);
        renderer.setUniformValues(getTextures(), getUniformValues());
        renderer.drawGeometry(getGeometry());    
        renderer.renderChildren(tree);
    }
  
    @Override
    public ISortToken getSortToken() {
        return sortToken;
    }
   
    @Override
    public Map<String, ShaderUniformValue> getUniformValues() {
        return uniforms;
    }
    
    
    public class SortToken implements ISortToken {
             
        @Override
        public boolean isTranslucent() {
            return renderConfig.isTranslucent();
        }
   
        @Override
        public double getZOrder() {
            return zOrder;
        }

        @Override
        public void setZOrder(double z) {
            zOrder = z;         
        }

    }

}
