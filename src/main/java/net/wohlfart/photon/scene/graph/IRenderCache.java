package net.wohlfart.photon.scene.graph;

import java.util.Collection;

import net.wohlfart.photon.renderer.Renderer.IRenderNode;

public interface IRenderCache {

    Tree<IRenderNode> getRoot();

    TreeImpl<IRenderNode> add(IRenderNode node);

    void removeAll(Collection<? extends IRenderNode> collection);

    void reOrder();

}
