package net.wohlfart.photon.scene.graph;

import java.util.HashMap;
import java.util.Map;

import net.wohlfart.photon.renderer.FrameBufferObject;
import net.wohlfart.photon.renderer.Geometry;
import net.wohlfart.photon.renderer.LwjglRenderConfig;
import net.wohlfart.photon.renderer.Renderer;
import net.wohlfart.photon.renderer.Renderer.IRenderEffect;
import net.wohlfart.photon.renderer.Renderer.IRenderNode;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.IGeometry.StreamFormat;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.scene.graph.NodeSortStrategy.ISortToken;
import net.wohlfart.photon.shader.ShaderIdentifier;
import net.wohlfart.photon.shader.ShaderTemplateWrapper;
import net.wohlfart.photon.shader.ShaderUniformHandle.ShaderUniformMatrixValue;
import net.wohlfart.photon.shader.ShaderUniformHandle.ShaderUniformValue;
import net.wohlfart.photon.texture.ITexture;
import net.wohlfart.photon.tools.Dimension;

import org.lwjgl.opengl.EXTFramebufferObject;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;

public class FboRenderTarget implements IRenderEffect {

    protected final ShaderIdentifier TEXTURE_SIMPLE_SHADER_ID = ShaderIdentifier
            .create("shader/texture/simple.vert", "shader/texture/simple.frag");

    protected final ShaderIdentifier TEXTURE_RADBLUR_SHADER_ID = ShaderIdentifier
            .create("shader/texture/radblur.vert", "shader/texture/radblur.frag");

    protected FrameBufferObject frameBufferObject = new FrameBufferObject();

    protected LwjglRenderConfig renderConfig = LwjglRenderConfig.BLENDING_ON;
    //protected LwjglRenderConfig renderConfig = LwjglRenderConfig.DEFAULT_3D;

    protected ISortToken sortToken = new SortToken();

    protected Map<String, ShaderUniformValue> uniforms = new HashMap<>();

    protected IGeometry geometry = createGeometry();
    
    protected Matrix4f m = new Matrix4f();

    protected double zOrder = Double.NaN;


    public FboRenderTarget() {
        uniforms.put(ShaderTemplateWrapper.UNIFORM_MODEL_2_WORLD_MTX, new ShaderUniformMatrixValue(m));
    }

    @Override
    public void accept(Renderer renderer, Tree<IRenderNode> tree) {
        // render on framebuffer
        bindFramebuffer(renderer);       
        renderer.renderChildren(tree);
        unbindFramebuffer(renderer);
        // render on screen
        renderer.renderChildren(tree);

        // rendering the quad last, maybe we need to put it in the render cache in order to be sorted...
        renderQuad(renderer);
    }

    private void bindFramebuffer(Renderer renderer) {
        FrameBufferObject frameBufferObject = getFrameBufferObject();
        if (!frameBufferObject.isInitialzed()) {
            frameBufferObject.setup(renderer.getScreenDimension());
        }            
        frameBufferObject.bind();    
    }

    private void unbindFramebuffer(Renderer visitor) {        
        EXTFramebufferObject.glBindFramebufferEXT(EXTFramebufferObject.GL_FRAMEBUFFER_EXT, 0);
        Dimension dim = visitor.getScreenDimension();
        GL11.glViewport(0, 0, dim.getWidth(), dim.getHeight());
    }

    private void renderQuad(Renderer renderer) {
        //visitor.setConfig(TEXTURE_SIMPLE_SHADER_ID, renderConfig);
        renderer.setConfig(TEXTURE_RADBLUR_SHADER_ID, renderConfig);

        final FrameBufferObject frameBufferObject = getFrameBufferObject();
        final Map<String, ITexture> textures = new HashMap<>();
        textures.put(ShaderTemplateWrapper.TEXTURE01, new ITexture() {
            @Override
            public int getHandle() {
                return frameBufferObject.getTextureHandle();
            }              
        });

        // TODO: fix m so the quad covers the whole view
        // m
        //System.out.println("m: " + m);
        Dimension dim = renderer.getScreenDimension();
        m.m00 = (float)dim.getWidth()/(float)dim.getHeight();
        m.m11 = 1;
        m.m22 = 1;
        m.m32 = -2.5f; // z transform      probabyl depends on the range of view...   
        
        renderer.setUniformValues(textures, uniforms);
        renderer.drawGeometry(geometry);
    }

    @Override
    public ISortToken getSortToken() {
        return sortToken;
    }

    @Override
    public FrameBufferObject getFrameBufferObject() {
        return frameBufferObject;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [getSortToken()=" + getSortToken() + "]";
    }

    private IGeometry createGeometry() {
        // see: http://lwjgl.org/wiki/index.php?title=The_Quad_textured
        // see: http://www.idevgames.com/forums/thread-1632.html for flipping y-axis
        Geometry geometry = new Geometry(VertexFormat.VERTEX_P3C0N0T2, StreamFormat.TRIANGLES);      
        geometry.addVertex().withPosition(+1,+1, 0).withTexture( 1, 1);
        geometry.addVertex().withPosition(-1,+1, 0).withTexture( 0, 1);
        geometry.addVertex().withPosition(-1,-1, 0).withTexture( 0, 0);
        geometry.addVertex().withPosition(+1,-1, 0).withTexture( 1, 0);        
        geometry.addRectangle(0, 1, 2, 3);                
        return geometry;
    }

    public class SortToken implements ISortToken {

        @Override
        public boolean isTranslucent() {
            return renderConfig.isTranslucent();
        }

        @Override
        public double getZOrder() {
            return zOrder;
        }

        @Override
        public void setZOrder(double z) {
            zOrder = z;
        }

    }

}
