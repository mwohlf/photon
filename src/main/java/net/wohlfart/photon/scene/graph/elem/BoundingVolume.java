package net.wohlfart.photon.scene.graph.elem;

import javax.vecmath.Vector3d;

import org.lwjgl.util.vector.Quaternion;


public interface BoundingVolume<T> {

    public void move(Vector3d transition);

    public void rotate(Quaternion transition);
    
    public BoundingVolumeSphere merge(T other);
    
    public boolean containsFully(T other);
    
    public boolean intersects(T other);

    public boolean intersects(Vector3d start, Vector3d end);

}
