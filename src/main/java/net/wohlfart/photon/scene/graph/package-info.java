package net.wohlfart.photon.scene.graph;

/**
 * 
 * class hierarchy:
 *   
 *   
 * render nodes:  
 *   
 *  - Renderer.IRenderNode:  minimum implementation for the double dispatch in the render loop to work
 *     
 *  - Renderer.IRenderUnit:  coplete data for rendering a VAO including shader, texture, model matrix etc
 * 
 * 
 * 
 * 
 * 
 * the scene graph is a collection of geometries, lights, sound, together 
 * with positional information, there are different ways to organize 
 * a scene so we might need different graphs focusing on different aspects 
 * of the scene (see: http://www.realityprime.com/blog/2007/06/scenegraphs-past-present-and-future/)
 * the graph(s) are traversed with the visitor pattern
 * 
 * aspect 1: culling
 * - we only need spatial objects with their bounding spheres to do a 
 *   check with the view frustum
 * - depth traversal (post-order) for bounding sphere calculation and
 *   frustum check  
 * - this is the boundingVolumeHierarchy
 * 
 * aspect 2: object updates (moving, rotating, inserting, removing)
 * - depth traversal (pre-order) for rotation propagation 
 *   (might be not needed if we have transformation/rotation nodes for partial trees)
 * 
 * aspect 3: optimizing state switched
 *  - minimize texture and shader switches
 *   
 * aspect 4: sorting for transparency
 * 
 *  
 * aspect 5:
 *  - reusing geometry data for some kind of objects if possible
 * 
 * 
 * 
 * datatypes for node elements
 *  - Group Nodes: Collects a number of nodes in a logical way
 *  - Level of Detail, Switch nodes: changing geometry depending on the distance
 *  - Transformation Nodes:  Moves, rotates, scales its children/siblings
 *  - Light Nodes: Make geometry's lit Pointlight, Spotlight, Directional, ...
 *  - Geometry nodes:  Usually a leaf node  Polygons, lines, points, ... (reusable)
 *  - Material: shader, texture (reusable)
 *  
 *  Traversers:
 *  Walks through the scenegraph, and operates on each node.
 *  RenderTraverser, CullTraverser,…
 * 
 *  Others
 *  Sound, fog, manipulators (animators), Collision, ...
 *  
 *  
 *  
 *  
 *  
 */

