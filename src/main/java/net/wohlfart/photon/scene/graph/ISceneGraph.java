package net.wohlfart.photon.scene.graph;

import java.util.Collection;

import javax.vecmath.Vector3d;

import net.wohlfart.photon.renderer.Renderer.IRenderNode;

import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;

public interface ISceneGraph {
    
    
    /**
     * semantic element of a scene graph
     */
    public interface IEntity3D {
        
        // current position, we use a 3d vector for this, TODO: scale the objects size later
        Vector3d getPosition();

        // rotation of the geometry
        Quaternion getRotation();
        
        // size, needed for culling, ray picking etc.
        float getSize();
          
        // incoming update for cam moves as well as moving the object
        void update(Quaternion rot, Vector3f mov, float delta);
       
        // register this object to the scene graph
        void register(ISceneGraph graph);
        
        // remove from the scene graph
        void unregister();
        
    }

 
    
    void addEntity(IEntity3D entity);

    void removeEntity(IEntity3D entity);

    void addRenderCommands(Collection<? extends IRenderNode> nodes);

    void removeRenderCommands(Collection<? extends IRenderNode> nodes);

    Tree<IRenderNode> createSubTree(IRenderNode effect);

}
