package net.wohlfart.photon.scene.graph.elem;

import javax.vecmath.Vector3d;

import org.lwjgl.util.vector.Quaternion;

public class BoundingVolumeSphere implements BoundingVolume<BoundingVolumeSphere> {
    
    // package private for testing
    final Vector3d center;
    final double radius;
    
    
    public BoundingVolumeSphere(Vector3d center, double radius) {
        this.center = center;
        this.radius = radius;
    }   
    
    @Override
    public void move(Vector3d transition) {
        center.add(transition);
    }

    @Override
    public void rotate(Quaternion transition) {
        // nothing to do for a sphere
    }

    @Override
    public BoundingVolumeSphere merge(BoundingVolumeSphere that) {
        double x,y,z,a,b,c,l;
        
        // vector from this center to the other center:  
        x = that.center.x - this.center.x;
        y = that.center.y - this.center.y;
        z = that.center.z - this.center.z;        
        l = Math.sqrt(x*x + y*y + z*z);
        
        if (Math.abs(l) < 0.00001) {
            // both have same center
            return new BoundingVolumeSphere(
                    new Vector3d(this.center.x, this.center.y, this.center.z), 
                    Math.max(this.radius, that.radius));
        }
        
        // vector to this border
        Vector3d start = new Vector3d(x,y,z);
        start.scale(-this.radius/l);
        start.add(this.center);
        
        // vector to that border
        Vector3d end = new Vector3d(x,y,z);
        end.scale(that.radius/l);
        end.add(that.center);
        
        x = (end.x + start.x)/2;
        y = (end.y + start.y)/2;
        z = (end.z + start.z)/2;
        
        a = end.x - start.x;
        b = end.y - start.y;
        c = end.z - start.z;
        
        l = Math.sqrt(a*a + b*b + c*c);       
        return new BoundingVolumeSphere(new Vector3d(x, y, z), l);
    }

    @Override
    public boolean containsFully(BoundingVolumeSphere that) {
        double x,y,z,l;
        
        // vector from this center to the other center:  
        x = that.center.x - this.center.x;
        y = that.center.y - this.center.y;
        z = that.center.z - this.center.z;        
        l = Math.sqrt(x*x + y*y + z*z);
       
        return this.radius >= l + that.radius;
    }

    @Override
    public boolean intersects(BoundingVolumeSphere that) {
        double x,y,z,l;
        
        // vector from this center to the other center:  
        x = that.center.x - this.center.x;
        y = that.center.y - this.center.y;
        z = that.center.z - this.center.z;        
        l = Math.sqrt(x*x + y*y + z*z);
        
        return l < this.radius + that.radius;
     }

    @Override
    public boolean intersects(Vector3d start, Vector3d end) {
        double x,y,z,l,a,b,c;
        
        // direction vector of the ray from start to end  
        x = end.x - start.x;
        y = end.y - start.y;
        z = end.z - start.z;        
        l = Math.sqrt(x*x + y*y + z*z);
        x /= l;
        y /= l;
        z /= l;

        // vector from center to the start of the ray
        a = center.x - start.x;
        b = center.y - start.y;
        c = center.z - start.z;      
        // dot product
        l = a*x + b*y + c*z;
        
        // find the base point, vector from start to base point
        x *= l;
        y *= l;
        z *= l;
        
        // finally the base point
        x -= a;
        y -= b;
        z -= c;
        
        x = center.x - x;
        y = center.y - y;
        z = center.z - z;      
        l = Math.sqrt(x*x + y*y + z*z);
             
        return l < radius;
    }
    
}
