package net.wohlfart.photon.scene.graph;

public interface Matcher<T> {
    
    boolean isMatch(T elem);

}
