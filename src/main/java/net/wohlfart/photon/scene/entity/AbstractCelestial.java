package net.wohlfart.photon.scene.entity;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import javax.vecmath.Vector3d;

import net.wohlfart.photon.renderer.LwjglRenderConfig;
import net.wohlfart.photon.scene.AbstractEntity3D;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;
import net.wohlfart.photon.shader.ShaderTemplateWrapper;
import net.wohlfart.photon.texture.ITexture;

import org.lwjgl.util.vector.Quaternion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/*
 * this class only needs a getRenderCommands() method 
 */
public abstract class AbstractCelestial extends AbstractEntity3D  {
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractCelestial.class);

    protected final Collection<AbstractRenderElement> renderCommands = new HashSet<>();


    @Override
    public Collection<AbstractRenderElement> getRenderCommands() {
        return renderCommands;
    }

    @Override
    public void destroy() {
        // nothing to do       
    }
    
  
    // -- setters
    
    @Override
    public AbstractCelestial withSize(float size) {
        super.withSize(size);
        return this;
    }
    
    @Override
    public AbstractCelestial withPosition(Vector3d position) {
        super.withPosition(position);
        return this;
    }

    @Override
    public AbstractCelestial withPosition(double x, double y, double z) {
        super.withPosition(x, y, z);
        return this;
    }

    @Override
    public AbstractCelestial withRotation(Quaternion rotation) {
        super.withRotation(rotation);
        return this;
    }
    
    public AbstractCelestial withCorona(Corona corona) {
        corona.setPlanetSize(size);
        renderCommands.add(corona);
        if (sceneGraph != null) {
            sceneGraph.addRenderCommands(Collections.singleton(corona));
        }
               
        return this;
    }
  
    protected static class RenderCommand extends AbstractRenderElement {

        public RenderCommand(IGeometry g, ITexture texture) {
            geometry = g;
            renderConfig = LwjglRenderConfig.DEFAULT_3D;
            textures.put(ShaderTemplateWrapper.TEXTURE01, texture);
            shaderId = TEXTURE_SIMPLE_SHADER_ID;
        }

        @Override
        public String toString() {
            return this.getClass().getName() + " [zOrder=" + zOrder
                    + " renderConfig=" + renderConfig + "]";
        }

    }

}
