package net.wohlfart.photon.scene.entity;

import static net.wohlfart.photon.ContextHolder.loadResource;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

import net.wohlfart.photon.renderer.Geometry;
import net.wohlfart.photon.scene.AbstractEntity3D;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.IGeometry.StreamFormat;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;
import net.wohlfart.photon.shader.ShaderTemplateWrapper;
import net.wohlfart.photon.texture.ITexture;
import net.wohlfart.photon.tools.MathTool;
import net.wohlfart.photon.tools.ObjectPool;
import net.wohlfart.photon.tools.OutOfResourcesException;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParticleEmitter extends AbstractEntity3D  {
    protected static final Logger LOGGER = LoggerFactory.getLogger(ParticleEmitter.class);

    protected final Collection<Particle> particles = new HashSet<>();
    
    protected final Vector3f velocity = new Vector3f(1f, 0f, 0f);
    
    protected final Vector3f acceleration = new Vector3f();


    private float leftover;

    protected final float newParticlePerSecond = 0.5f;
    protected final float particleLifetime = 5f;
    protected final int maxParticleCount = (int) Math.ceil(newParticlePerSecond * particleLifetime);

    private final ObjectPool<Particle> pool = new ObjectPool<Particle>(maxParticleCount) {
        @Override
        protected Particle newObject() {
            return new Particle();
        }
    };

    
    @Override
    public Collection<Particle> getRenderCommands() {
        return particles;
    }
 
    @Override
    public void setup() {
        // nothing to do
    }

    @Override
    public void destroy() {
        // nothing to do
    }
    
  
    @Override
    public void update(Quaternion rot, Vector3f mov, float delta) {
        super.update(rot, mov, delta);                

        MathTool.multLocal(rot, velocity);    
        MathTool.multLocal(rot, acceleration);    

        destroyAndCreate(delta);
        
        // re-position existing particles (they have their own speed/acceleration)
        for (Particle particle : particles) {
            particle.update(rot, mov, delta);
        } 
                           
    };
    
    
    private void destroyAndCreate(float delta) {
        
        // destroy and return to the pool 
        Iterator<Particle> iter = particles.iterator();
        while (iter.hasNext()) {
            Particle particle = iter.next();          
            if (particle.willExpire(delta)) {
                iter.remove();            
                sceneGraph.removeRenderCommands(Collections.singleton(particle));
                particle.reset();
            }
        }

        // create new 
        leftover += delta * newParticlePerSecond;
        int newCount = (int) leftover;
        leftover = leftover - newCount;
        Collection<Particle> newElems = createParticles(newCount);
        particles.addAll(newElems);
        sceneGraph.addRenderCommands(newElems);
    }
    

    private Collection<Particle> createParticles(int count) {
        Collection<Particle> result = new HashSet<Particle>();
        try {
            for (int i = 0; i < count; i++) {
                result.add(createSingleParticle());
            }     
        }
        catch (OutOfResourcesException ex) {
            LOGGER.warn("recover from exception", ex);  // FIXME: ask the pool instead of catching an exception
        }
        return result;
    }

    private Particle createSingleParticle() {
        Particle particle = pool.borrowObject();
        particle.position = new Vector3f((float)position.x, (float)position.y, (float)position.z);
        // particle.velocity = new Vector3f(MathTool.random(-0.5f, 0.5f), 3f, MathTool.random(-0.5f, 0.5f)); // current speed
        particle.velocity = new Vector3f(velocity); // current speed
        // particle.acceleration = new Vector3f(0, -0.3f, 0);
        particle.acceleration = new Vector3f(acceleration);;
        particle.expire = particleLifetime;
        return particle;
    }

    private IGeometry createGeometry() {
        // see: http://lwjgl.org/wiki/index.php?title=The_Quad_textured
        Geometry geometry = new Geometry(VertexFormat.VERTEX_P3C0N0T2, StreamFormat.TRIANGLES);      
        geometry.addVertex().withPosition(+1,+1, 0).withTexture( 1, 0);
        geometry.addVertex().withPosition(-1,+1, 0).withTexture( 0, 0);
        geometry.addVertex().withPosition(-1,-1, 0).withTexture( 0, 1);
        geometry.addVertex().withPosition(+1,-1, 0).withTexture( 1, 1);        
        geometry.addRectangle(0, 1, 2, 3);                
        //geometry.transformVertices(VertexTransform.move(0,0,-4));               
        return geometry;
    }

    
    public class Particle extends AbstractRenderElement implements ObjectPool.PoolableObject  {
        
        private final Vector3f a = new Vector3f();
        private final Vector3f v = new Vector3f();

        private float expire;        // [s]
        private Vector3f velocity;     // [m/s]
        private Vector3f acceleration; // [m/s^2]

        private Vector3f position = new Vector3f();     // [m]
        protected final Quaternion rotation = new Quaternion();


        public Particle() {
            geometry = createGeometry();
            shaderId = TEXTURE_SIMPLE_SHADER_ID;
            textures.put(ShaderTemplateWrapper.TEXTURE01, loadResource(ITexture.class, TEXTURE_ID1));
        }

        public boolean willExpire(float delta) {
            expire -= delta;
            return (expire - delta) <= 0;
        }

        public void update(Quaternion rot, Vector3f mov, float delta) {

            Quaternion.mul(rot, rotation, rotation);

            position.x += mov.x;
            position.y += mov.y;
            position.z += mov.z;     

            MathTool.multLocal(rot, position); 
            MathTool.multLocal(rot, velocity); 
            MathTool.multLocal(rot, acceleration); 


            a.set(acceleration);
            a.scale(delta);
            velocity = Vector3f.add(velocity, a, velocity);

            v.set(velocity);
            v.scale(delta);
            position = Vector3f.add(position, v, position);

            Matrix4f m = getModel2WorldMatrix();
            MathTool.convert(rotation, m);
            m.m30 = (float) position.x;
            m.m31 = (float) position.y;
            m.m32 = (float) position.z;
            m.m33 = (float) 1;
            
            faceOrigin(position, rotation);
        }

        private void faceOrigin(Vector3f position2, Quaternion rotation) {
            MathTool.createQuaternion(MathTool.Z_AXIS, position2, rotation);
        }

        @Override
        public void reset() {
            pool.returnObject(this);
        }
        
        @Override
        public String toString() {
            return this.getClass().getSimpleName() + " [zOrder=" + zOrder 
                    + "renderConfig=" + renderConfig + "]";
        }

    }

}
