package net.wohlfart.photon.scene.entity;

import net.wohlfart.photon.renderer.Geometry;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.IGeometry.StreamFormat;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.scene.IVertexTransform;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;

public class WireCube extends AbstractRenderElement { // NO_UCD (public API)

    public WireCube(float length) {
        geometry = createGeometry(length);
    }
    
    private IGeometry createGeometry(float length) {
        Geometry geometry = new Geometry(VertexFormat.VERTEX_P3C0N0T0, StreamFormat.LINES);
        final float l = length / 2f;

        geometry.addVertex().withPosition(+1, +1, +1);
        geometry.addVertex().withPosition(-1, +1, +1);
        geometry.addVertex().withPosition(-1, -1, +1);
        geometry.addVertex().withPosition(+1, -1, +1);

        geometry.addVertex().withPosition(+1, +1, -1);
        geometry.addVertex().withPosition(-1, +1, -1);
        geometry.addVertex().withPosition(-1, -1, -1);
        geometry.addVertex().withPosition(+1, -1, -1);

        geometry.addLine(0, 1);
        geometry.addLine(1, 2);
        geometry.addLine(2, 3);
        geometry.addLine(3, 0);

        geometry.addLine(0, 4);
        geometry.addLine(1, 5);
        geometry.addLine(2, 6);
        geometry.addLine(3, 7);

        geometry.addLine(4, 5);
        geometry.addLine(5, 6);
        geometry.addLine(6, 7);
        geometry.addLine(7, 4);

        geometry.transformVertices(new IVertexTransform() {
            @Override
            public float[] execute(VertexFormat format, float[] vertexElement) {
                vertexElement[0] = (vertexElement[0] * l) + 3; // moving right
                vertexElement[1] = (vertexElement[1] * l);
                vertexElement[2] = (vertexElement[2] * l)- 10; // moving back
                return vertexElement;
            }

        });       
        return geometry;
    }
    
}
