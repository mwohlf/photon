package net.wohlfart.photon.scene.entity;

import static net.wohlfart.photon.ContextHolder.loadResource;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.IGeometry.StreamFormat;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.scene.geometry.SphereGeometry;
import net.wohlfart.photon.texture.CelestialType;
import net.wohlfart.photon.texture.ITexture;
import net.wohlfart.photon.texture.TextureIdentifier;

public class ProceduralCelestial extends AbstractCelestial {

    protected CelestialType type;

    @Override
    public void setup() {
        lod = 6;
        final TextureIdentifier textureId = TextureIdentifier.create(40, type, 2);
        final IGeometry geometry = new SphereGeometry(getSize(), lod, VertexFormat.VERTEX_P3C0N0T2, StreamFormat.TRIANGLES);
        final RenderCommand renderUnit = new RenderCommand(geometry, loadResource(ITexture.class, textureId));
        renderCommands.add(renderUnit);
    }

    public ProceduralCelestial withType(CelestialType type) {
        this.type = type;
        return this;
    }

}
