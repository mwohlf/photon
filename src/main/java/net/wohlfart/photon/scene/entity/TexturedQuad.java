package net.wohlfart.photon.scene.entity;

import static net.wohlfart.photon.ContextHolder.loadResource;
import net.wohlfart.photon.renderer.Geometry;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.IGeometry.StreamFormat;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.scene.IVertexTransform;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;
import net.wohlfart.photon.shader.ShaderTemplateWrapper;
import net.wohlfart.photon.texture.ITexture;

public class TexturedQuad extends AbstractRenderElement {
    
    public TexturedQuad() {
        geometry = createGeometry();
        textures.put(ShaderTemplateWrapper.TEXTURE01, loadResource(ITexture.class, TEXTURE_ID1));
        shaderId = TEXTURE_SIMPLE_SHADER_ID;
    }
  
    private IGeometry createGeometry() {
        // see: http://lwjgl.org/wiki/index.php?title=The_Quad_textured
        Geometry geometry = new Geometry(VertexFormat.VERTEX_P3C0N0T2, StreamFormat.TRIANGLES);      
        geometry.addVertex().withPosition(+1,+1, 0).withTexture( 1, 0);
        geometry.addVertex().withPosition(-1,+1, 0).withTexture( 0, 0);
        geometry.addVertex().withPosition(-1,-1, 0).withTexture( 0, 1);
        geometry.addVertex().withPosition(+1,-1, 0).withTexture( 1, 1);        
        geometry.addRectangle(0, 1, 2, 3);                
        geometry.transformVertices(new IVertexTransform() {
            @Override
            public float[] execute(VertexFormat format, float[] vertexElement) {
                vertexElement[2] -= 4; // z coord
                return vertexElement;
            }
        });
        return geometry;
    }
   
    
}
