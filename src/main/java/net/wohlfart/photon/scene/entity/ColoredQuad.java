package net.wohlfart.photon.scene.entity;

import net.wohlfart.photon.renderer.Geometry;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.IGeometry.StreamFormat;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;

import org.lwjgl.util.ReadableColor;

public class ColoredQuad extends AbstractRenderElement { // NO_UCD (public API)

    public ColoredQuad() {
        geometry = createGeometry(4f);
    }
    
    public IGeometry createGeometry(float length) {
        ReadableColor red = ReadableColor.RED;
        ReadableColor green = ReadableColor.GREEN;
        ReadableColor blue = ReadableColor.BLUE;
        ReadableColor yellow = ReadableColor.YELLOW;
        float[] redArray = {red.getRed() / 256f, red.getGreen() / 256f, red.getBlue() / 256f, red.getAlpha() / 256f};
        float[] greenArray = {green.getRed() / 256f, green.getGreen() / 256f, green.getBlue() / 256f, green.getAlpha() / 256f};
        float[] blueArray = {blue.getRed() / 256f, blue.getGreen() / 256f, blue.getBlue() / 256f, blue.getAlpha() / 256f};
        float[] yellowArray = {yellow.getRed() / 256f, yellow.getGreen() / 256f, yellow.getBlue() / 256f, yellow.getAlpha() / 256f};
        Geometry geometry = new Geometry(VertexFormat.VERTEX_P3C4N0T0, StreamFormat.TRIANGLES);

        geometry.addVertex().withPosition(+1,+1, 0).withColor(redArray);
        geometry.addVertex().withPosition(-1,+1, 0).withColor(greenArray);
        geometry.addVertex().withPosition(-1,-1, 0).withColor(blueArray);
        geometry.addVertex().withPosition(+1,-1, 0).withColor(yellowArray);
        geometry.addRectangle(0, 1, 2, 3);

        return geometry;
    }

}
