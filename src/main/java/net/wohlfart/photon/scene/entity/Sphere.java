package net.wohlfart.photon.scene.entity;

import net.wohlfart.photon.scene.geometry.SphereGeometry;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;

public class Sphere extends AbstractRenderElement {

    public Sphere() {
        geometry = new SphereGeometry(4, 6);
    }
}
