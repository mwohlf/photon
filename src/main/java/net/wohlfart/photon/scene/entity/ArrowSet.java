package net.wohlfart.photon.scene.entity;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import javax.vecmath.Vector3d;

import net.wohlfart.photon.renderer.Geometry;
import net.wohlfart.photon.renderer.LwjglRenderConfig;
import net.wohlfart.photon.scene.AbstractEntity3D;
import net.wohlfart.photon.scene.IGeometry.StreamFormat;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.scene.IVertexTransform;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;
import net.wohlfart.photon.tools.MathTool;

import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ArrowSet extends AbstractEntity3D {
    protected static final Logger LOGGER = LoggerFactory.getLogger(ArrowSet.class);

    protected final Collection<Arrow> arrows = new HashSet<>();

    
    @Override
    public Collection<Arrow> getRenderCommands() {
        return arrows;
    }

    @Override
    public void setup() {
        // nothing to do       
    }

    @Override
    public void destroy() {
        // nothing to do
    }
  
    public void addArrow(Arrow arrow) {
        // the arrow is is the origin/cam coordinate system, needs to be aligned with this entities coordinate system
              
        final Vector3d pos = getPosition();   // TODO: check the size of the vec3d too big for float means this whole entity is becoming too big
        arrow.getGeometry().transformVertices(new IVertexTransform() {
            @Override
            public float[] execute(VertexFormat format, float[] vertElem) {
                vertElem[0] -= (float) pos.x;
                vertElem[1] -= (float) pos.y;
                vertElem[2] -= (float) pos.z;
                return vertElem;
            }
        });
           
        final Quaternion rot = Quaternion.negate(this.getRotation(), new Quaternion());
        arrow.getGeometry().transformVertices(new IVertexTransform() {
            @Override
            public float[] execute(VertexFormat format, float[] vertElem) {
                final Vector3f vec = new Vector3f();
                MathTool.mul(rot, new Vector3f(vertElem[0], vertElem[1], vertElem[2]), vec);
                vertElem[0] = vec.x;
                vertElem[1] = vec.y;
                vertElem[2] = vec.z;
                return vertElem;
            }
        });
        
        arrows.add(arrow);       
        sceneGraph.addRenderCommands(Collections.singleton(arrow));
    }
       
    public static class Arrow extends AbstractRenderElement {

        private Arrow(final Vector3f tip) {
            renderConfig = LwjglRenderConfig.DEFAULT_3D;
            setupNormalizedGeometry();
            
            final Quaternion rotation = new Quaternion();
            MathTool.createQuaternion(new Vector3f(0, 0, 1), tip, rotation);
            
            getGeometry().transformVertices(new IVertexTransform() {
                @Override
                public float[] execute(VertexFormat format, float[] vertElem) {
                    Vector3f vert = new Vector3f();
                    MathTool.mul(rotation, new Vector3f(vertElem[0], vertElem[1], vertElem[2]), vert);
                    MathTool.mul(vert, tip.length());
                    vertElem[0] = vert.x;
                    vertElem[1] = vert.y;
                    vertElem[2] = vert.z;
                    return vertElem;
                }
            });
        }
        
        public Arrow(final Vector3f start, Vector3f end) {
            this(new Vector3f(end.x - start.x, end.y - start.y, end.z - start.z));

            getGeometry().transformVertices(new IVertexTransform() {
                @Override
                public float[] execute(VertexFormat format, float[] vertElem) {
                    vertElem[0] += start.x;
                    vertElem[1] += start.y;
                    vertElem[2] += start.z;
                    return vertElem;
                }
            });
            
            zOrder = Math.sqrt(start.x * start.x + start.y * start.y + start.z * start.z);
        }

        @Override
        public String toString() {
            return this.getClass().getName() + " [zOrder=" + zOrder
                        + " renderConfig=" + renderConfig + "]";
        }
        
        @Override
        public Geometry getGeometry() {
            return (Geometry) geometry;
        }

        private void setupNormalizedGeometry() {
            Geometry geometry = new Geometry(VertexFormat.VERTEX_P3C0N0T0, StreamFormat.LINES);
            geometry.addVertex().withPosition(+0.00f, +0.00f, +1.00f);
            geometry.addVertex().withPosition(+0.00f, +0.00f, +0.00f);
            geometry.addVertex().withPosition(+0.02f, +0.02f, +0.90f);
            geometry.addVertex().withPosition(-0.02f, +0.02f, +0.90f);
            geometry.addVertex().withPosition(-0.02f, -0.02f, +0.90f);
            geometry.addVertex().withPosition(+0.02f, -0.02f, +0.90f);
            
            geometry.addLine(1, 0);
            geometry.addLine(2, 0);
            geometry.addLine(3, 0);
            geometry.addLine(4, 0);
            geometry.addLine(5, 0);     
            this.geometry = geometry;
        }
        
    }

}