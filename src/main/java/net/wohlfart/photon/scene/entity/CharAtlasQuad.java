package net.wohlfart.photon.scene.entity;

import static net.wohlfart.photon.ContextHolder.loadResource;
import net.wohlfart.photon.hud.txt.CharAtlasFactory;
import net.wohlfart.photon.hud.txt.ICharAtlas;
import net.wohlfart.photon.renderer.Geometry;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.IGeometry.StreamFormat;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.scene.IVertexTransform;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;
import net.wohlfart.photon.shader.ShaderTemplateWrapper;
import net.wohlfart.photon.texture.ImageTexture;

public class CharAtlasQuad extends AbstractRenderElement { // NO_UCD (public API)
      
    public CharAtlasQuad() {
        geometry = setupGeometry();        
        ICharAtlas charAtlas = loadResource(ICharAtlas.class, CharAtlasFactory.DEFAULT_FONT_ID);
        textures.put(ShaderTemplateWrapper.TEXTURE01, new ImageTexture(charAtlas.getImage()));
        shaderId = TEXTURE_SIMPLE_SHADER_ID;
    }
    
    private IGeometry setupGeometry() {

        // see: http://lwjgl.org/wiki/index.php?title=The_Quad_textured
        Geometry geometry = new Geometry(VertexFormat.VERTEX_P3C0N0T2, StreamFormat.TRIANGLES);      
        geometry.addVertex().withPosition(+1,+1, 0).withTexture( 1, 0);
        geometry.addVertex().withPosition(-1,+1, 0).withTexture( 0, 0);
        geometry.addVertex().withPosition(-1,-1, 0).withTexture( 0, 1);
        geometry.addVertex().withPosition(+1,-1, 0).withTexture( 1, 1);        
        geometry.addRectangle(0, 1, 2, 3);                
        //geometry.addRectangle(3,2,1,0);                
        geometry.transformVertices(new IVertexTransform() {
            @Override
            public float[] execute(VertexFormat format, float[] vertexElement) {
                vertexElement[2] -= 2; // z coord
                return vertexElement;
            }

        });
        
        return geometry;
    }
      
}
