package net.wohlfart.photon.scene.entity;

import net.wohlfart.photon.renderer.LwjglRenderConfig;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.geometry.SphereGeometry;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;

public class Corona extends AbstractRenderElement {

    private float planetSize;
    private float thinkness;


    public Corona() {
        renderConfig = LwjglRenderConfig.DEFAULT_3D;
        shaderId = SIMPLE_SHADER_ID;
    }

    @Override // lazy create the geometry since we don't know the size in the constructor
    public IGeometry getGeometry() {
        if (geometry == null) {
            geometry = new SphereGeometry(planetSize + thinkness, 6);
        }
        return geometry;
    }

    public void setPlanetSize(float size) {
        planetSize = size;
        geometry = null;
    }

    public Corona withThinkness(float thinkness) {
        this.thinkness = thinkness;
        geometry = null;
        return this;
    }


    @Override
    public String toString() {
        return this.getClass().getName() + " [zOrder=" + zOrder
                + " renderConfig=" + renderConfig 
                + " planetSize=" + planetSize             
                + " thinkness=" + thinkness             
                + "]";
    }

}
