package net.wohlfart.photon.scene.entity;

import net.wohlfart.photon.scene.geometry.TetrahedronGeometry;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;

public class Tetrahedron extends AbstractRenderElement { // NO_UCD (public API)

    public Tetrahedron() {
        geometry = new TetrahedronGeometry();
    }
    
}
