package net.wohlfart.photon.scene.entity;

import net.wohlfart.photon.renderer.Geometry;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.IGeometry.StreamFormat;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.scene.IVertexTransform;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;
import net.wohlfart.photon.tools.MathTool;

public class Circle extends AbstractRenderElement { // NO_UCD (public API)
    
    public Circle(float radius, int pieces) {
        geometry = setupGeometry(radius, pieces);
    }
    
    private IGeometry setupGeometry(float radius, int pieces) {
        Geometry geometry = new Geometry(VertexFormat.VERTEX_P3C0N0T0, StreamFormat.LINE_LOOP);
        for (int i = 0; i < pieces; i++) {
            final float rad = MathTool.TWO_PI * i / pieces;
            final float x = MathTool.sin(rad) * radius;
            final float y = MathTool.cos(rad) * radius;
            geometry.addVertex().withPosition(x, y, 0);
        }
        geometry.transformVertices(new IVertexTransform() {
            @Override
            public float[] execute(VertexFormat format, float[] vertexElement) {
                vertexElement[0] = vertexElement[0] - 3; // moving left
                vertexElement[1] = vertexElement[1];
                vertexElement[2] = vertexElement[2] - 30; // moving back
                return vertexElement;
            }

        });
        return geometry;
    }

}
