package net.wohlfart.photon.scene.entity;

import net.wohlfart.photon.renderer.Geometry;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.IGeometry.StreamFormat;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;
import net.wohlfart.photon.tools.MathTool;

import org.lwjgl.util.ReadableColor;

public class ColoredCircle extends AbstractRenderElement { // NO_UCD (public API)
   
    public ColoredCircle(float radius, int pieces, ReadableColor color) {
        geometry = setupGeometry(radius, pieces, color);
    }
    
    private IGeometry setupGeometry(float radius, int pieces, ReadableColor color) {
        Geometry geometry = new Geometry(VertexFormat.VERTEX_P3C4N0T0, StreamFormat.LINE_LOOP);
        for (int i = 0; i < pieces; i++) {
            final float rad = MathTool.TWO_PI * i / pieces;
            final float x = MathTool.sin(rad) * radius;
            final float y = MathTool.cos(rad) * radius;
            geometry.addVertex()
            .withPosition(x, y, -50)
            .withColor(color.getRed() / 256f, color.getGreen() / 256f, color.getBlue() / 256f, color.getAlpha() / 256f);
        }
        return geometry;
    }

}
