package net.wohlfart.photon.scene.entity;

import net.wohlfart.photon.scene.geometry.IcosphereGeometry;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;

public class Icosphere extends AbstractRenderElement { // NO_UCD (public API)

    public Icosphere() {
        geometry = new IcosphereGeometry();
    }
}
