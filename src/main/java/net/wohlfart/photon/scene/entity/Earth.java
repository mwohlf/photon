package net.wohlfart.photon.scene.entity;

import static net.wohlfart.photon.ContextHolder.loadResource;
import net.wohlfart.photon.scene.IGeometry.StreamFormat;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.scene.geometry.SphereGeometry;
import net.wohlfart.photon.texture.ITexture;
import net.wohlfart.photon.texture.ITexture.ITextureIdentifier;
import net.wohlfart.photon.texture.TextureIdentifier;

@SuppressWarnings("unused")
public class Earth extends AbstractCelestial {

    private static final TextureIdentifier EARTH_LOWRES =  TextureIdentifier.create("gfx/textures/earth-512.jpg");

    private static final ITextureIdentifier EARTH_HIRES =  TextureIdentifier.create("gfx/textures/earth-1024.jpg");

    @Override
    public void setup() {
        lod = 6;
        //assert renderCommands.isEmpty();
        renderCommands.add(new RenderCommand(
                new SphereGeometry(5, lod, VertexFormat.VERTEX_P3C0N0T2, StreamFormat.TRIANGLES),
                loadResource(ITexture.class, EARTH_HIRES)));
    }
    
}
