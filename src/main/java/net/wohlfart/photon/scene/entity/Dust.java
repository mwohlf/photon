package net.wohlfart.photon.scene.entity;

import static net.wohlfart.photon.ContextHolder.loadResource;

import java.util.Collection;
import java.util.HashSet;

import javax.vecmath.Vector3d;

import net.wohlfart.photon.ContextHolder;
import net.wohlfart.photon.Settings;
import net.wohlfart.photon.renderer.Geometry;
import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.IGeometry.StreamFormat;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.scene.VertexTransform;
import net.wohlfart.photon.scene.graph.AbstractRenderElement;
import net.wohlfart.photon.scene.graph.ISceneGraph;
import net.wohlfart.photon.scene.graph.ISceneGraph.IEntity3D;
import net.wohlfart.photon.shader.ShaderTemplateWrapper;
import net.wohlfart.photon.texture.ITexture;
import net.wohlfart.photon.tools.MathTool;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;

public class Dust implements IEntity3D {

    protected ISceneGraph sceneGraph;

    protected final Collection<Dust.Particle> particles = new HashSet<>(6);

    protected final float particleCount = 1000f;

    protected float forwardCull; // in -z direction, any particle further away will be culled and reused

    protected float xCull;
    
    protected float yCull;

    @Override
    public void update(Quaternion rot, Vector3f mov, float delta) {       
        for (Particle particle : particles) {
            particle.update(rot, mov, delta);
        }        
    }

    @Override
    public void register(ISceneGraph sceneGraph) {
        this.sceneGraph = sceneGraph;
        final Settings settings = ContextHolder.getSettings();
        float fieldOfView = settings.getFieldOfView();
        float w = settings.getWidth();
        float h = settings.getHeight();
        
        forwardCull = settings.getFarPlane()/2f;
        xCull = MathTool.tan(MathTool.deg2rad(fieldOfView/2f)) * forwardCull * w/h;
        yCull = MathTool.tan(MathTool.deg2rad(fieldOfView/2f)) * forwardCull;

        setup(); 
        sceneGraph.addEntity(this);
        sceneGraph.addRenderCommands(particles);        
    }

    @Override
    public void unregister() {
        destroy();
        sceneGraph.removeRenderCommands(particles);
        sceneGraph.removeEntity(this);
        sceneGraph = null;        
    }   

    private void setup() {

        for (int i = 0; i < particleCount; i++) {
            particles.add(createSingleParticle());
        }
    }   

    private Particle createSingleParticle() {
        Particle particle = new Particle();
        particle.position.x = MathTool.random(-xCull, xCull);
        particle.position.y = MathTool.random(-yCull, yCull);
        particle.position.z = MathTool.random(-forwardCull, 0);         
        return particle;
    }

    private void destroy() {
        particles.clear();
    }

    @Override
    public Vector3d getPosition() {
        throw new IllegalAccessError("getPosition not supported for Dust");
    }

    @Override
    public Quaternion getRotation() {
        throw new IllegalAccessError("getRotation not supported for Dust");
    }

    @Override
    public float getSize() {
        throw new IllegalAccessError("getSize not supported for Dust");
    }

    private IGeometry createGeometry() {
        // see: http://lwjgl.org/wiki/index.php?title=The_Quad_textured
        Geometry geometry = new Geometry(VertexFormat.VERTEX_P3C0N0T2, StreamFormat.TRIANGLES);      
        geometry.addVertex().withPosition(+1,+1, 0).withTexture( 1, 0);
        geometry.addVertex().withPosition(-1,+1, 0).withTexture( 0, 0);
        geometry.addVertex().withPosition(-1,-1, 0).withTexture( 0, 1);
        geometry.addVertex().withPosition(+1,-1, 0).withTexture( 1, 1);        
        geometry.addRectangle(0, 1, 2, 3);                
        geometry.transformVertices(VertexTransform.move(0,0,-4));               
        return geometry;
    }

    public class Particle extends AbstractRenderElement {

        protected final Vector3f position = new Vector3f();

        protected final Quaternion rotation = new Quaternion();


        public Particle() {
            geometry = createGeometry();
            shaderId = TEXTURE_SIMPLE_SHADER_ID;
            textures.put(ShaderTemplateWrapper.TEXTURE01, loadResource(ITexture.class, TEXTURE_ID1));
        }

        public void update(Quaternion rot, Vector3f mov, float delta) {

            position.x += mov.x;
            position.y += mov.y;            
            position.z += mov.z;


            MathTool.multLocal(rot, position); 

            Matrix4f m = getModel2WorldMatrix();
            MathTool.convert(rotation, m);
            m.m30 = (float) position.x;
            m.m31 = (float) position.y;
            m.m32 = (float) position.z;
            m.m33 = (float) 1;

            doCulling(m);
            
            position.x = m.m30;
            position.y = m.m31;
            position.z = m.m32;
            
            faceOrigin(position, rotation);
        }

        private void faceOrigin(Vector3f position2, Quaternion rotation) {
            MathTool.createQuaternion(MathTool.Z_AXIS, position2, rotation);
        }

        private void doCulling(Matrix4f m) {

            // x-range culling 
            while (m.m30 < -xCull) {   // too far left
                m.m30 = (m.m30 + xCull*2);  // bring it in from the right
            }
            while (m.m30 > +xCull) {   // too far right
                m.m30 = (m.m30 - xCull*2);
            }

            // y-range culling
            while (m.m31 < -yCull) {
                m.m31 = (m.m31 + yCull*2);
            }
            while (m.m31 > +yCull) {
                m.m31 = (m.m31 - yCull*2);
            }

            // z-range culling
            while (m.m32 < -forwardCull) {
                m.m32 = (m.m32 + forwardCull);
            }
            while (m.m32 > 0) {
                m.m32 = (m.m32 - forwardCull);
            }

        }

        @Override
        public String toString() {
            return this.getClass().getSimpleName() + " [zOrder=" + zOrder 
                    + " renderConfig=" + renderConfig + "]";
        }

    }


}
