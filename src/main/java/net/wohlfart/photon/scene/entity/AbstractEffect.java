package net.wohlfart.photon.scene.entity;


import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.vecmath.Vector3d;

import net.wohlfart.photon.renderer.Renderer.IRenderNode;
import net.wohlfart.photon.scene.AbstractEntity3D;
import net.wohlfart.photon.scene.graph.FboRenderTarget;
import net.wohlfart.photon.scene.graph.ISceneGraph;
import net.wohlfart.photon.scene.graph.ISceneGraph.IEntity3D;
import net.wohlfart.photon.scene.graph.Tree;

import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;

public abstract class AbstractEffect implements IEntity3D  {

    private final FboRenderTarget overlay = new FboRenderTarget();

    private final Set<AbstractEntity3D> childEntities = new HashSet<>();

    private ISceneGraph sceneGraph;

    private Tree<IRenderNode> tree;


    @Override  
    public void register(ISceneGraph sceneGraph) {
        this.sceneGraph = sceneGraph;
        sceneGraph.addEntity(this);   
        tree = sceneGraph.createSubTree(overlay);
        for (AbstractEntity3D entity : childEntities) {
            entity.setup();
            for (IRenderNode command : entity.getRenderCommands()) {
                tree.add(command);
            }
        }
    }

    @Override  
    public void unregister() {
        assert sceneGraph != null : "need to call register before you can call unregister";
        sceneGraph.removeEntity(this);
        sceneGraph.removeRenderCommands(Collections.singleton(overlay));
    }

    public void addEntity(AbstractEntity3D entity) {
        childEntities.add(entity);
        if (tree != null) {
            entity.setup();
            for (IRenderNode command : entity.getRenderCommands()) {
                tree.add(command);
            }
        }
    }

    @Override
    public void update(Quaternion rot, Vector3f mov, float delta) {      
        for (AbstractEntity3D entity : childEntities) {
            entity.update(rot, mov, delta);
        }
    }

    @Override
    public Vector3d getPosition() {
        throw new IllegalAccessError("getPosition not supported");
    }

    @Override
    public Quaternion getRotation() {
        throw new IllegalAccessError("getRotation not supported");
    }

    @Override
    public float getSize() {
        throw new IllegalAccessError("getSize not supported");
    }

}
