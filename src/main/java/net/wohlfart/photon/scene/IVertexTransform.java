package net.wohlfart.photon.scene;

import net.wohlfart.photon.scene.IGeometry.VertexFormat;

public interface IVertexTransform {

    public IVertexTransform IDENTITY_TRANSFORM = new IVertexTransform() {
        @Override
        public float[] execute(VertexFormat format, float[] vertexElement) {
            return vertexElement;
        }
    };

    float[] execute(VertexFormat format, float[] vertexElement);

}
