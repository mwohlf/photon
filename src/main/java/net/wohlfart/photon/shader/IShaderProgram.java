package net.wohlfart.photon.shader;

import java.net.URI;
import java.util.Map;
import java.util.Set;

import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.shader.ShaderUniformHandle.ShaderUniformValue;

public interface IShaderProgram {
    

    public interface IShaderProgramIdentifier {
        
        URI getVertexShaderResource();
        
        URI getFragmentShaderResource();
        
    }

    void setup();

    void bind();

    void unbind();

    void dispose();

    void setupAttributes(VertexFormat vertexFormat);

    void setUniform(String uniformName, int textureSlot);

    void updateShadersUniforms(Set<String> textureNames, Map<String, ShaderUniformValue> uniformValues);

    
    
    public class NullShader implements IShaderProgram {

        @Override
        public void setup() {
            // do nothing
        }

        @Override
        public void bind() {
            // do nothing
        }

        @Override
        public void unbind() {
            // do nothing
        }

        @Override
        public void dispose() {
            // do nothing
        }

        @Override
        public void setupAttributes(VertexFormat vertexFormat) {
            // do nothing
       }

        @Override
        public void setUniform(String uniformName, int textureSlot) {
            // do nothing
       }

        @Override
        public void updateShadersUniforms(Set<String> textureNames, Map<String, ShaderUniformValue> uniformValues) {
            // do nothing
       }

    }
}
