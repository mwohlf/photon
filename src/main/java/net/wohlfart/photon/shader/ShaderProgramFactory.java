package net.wohlfart.photon.shader;

import net.wohlfart.photon.resources.ResourceProducer;
import net.wohlfart.photon.shader.IShaderProgram.IShaderProgramIdentifier;

public class ShaderProgramFactory implements ResourceProducer<IShaderProgram, IShaderProgram.IShaderProgramIdentifier>{

    @Override
    public IShaderProgram produce(IShaderProgramIdentifier identifier) {
        return new LwjglShaderProgram(identifier);
    }

}
