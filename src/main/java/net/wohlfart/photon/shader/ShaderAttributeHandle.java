package net.wohlfart.photon.shader;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL21;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL40;

public class ShaderAttributeHandle {

    private final int shaderProgramId;

    private final String name;
    private final int location;
    
    private final int typeSize;
    @SuppressWarnings("unused")
    private final int typeId;
    
    @SuppressWarnings("unused")
    private String type;
    private int attributeSize; // in floats
    

    ShaderAttributeHandle(int shaderProgramId, String name, int typeSize, int typeId, int location) {
        this.shaderProgramId = shaderProgramId;
        this.name = name;
        this.typeSize = typeSize;
        this.typeId = typeId;
        this.location = location;
        setupTypeInfo(typeId, typeSize);
    }

    public int getAttributeSize() {
        return attributeSize;
    }
    
    public void enable(int size, int stride, int offset) {
        GL20.glEnableVertexAttribArray(location);
        GL20.glVertexAttribPointer(location, size, GL11.GL_FLOAT, false, stride, offset);
    }

    /**
     * disable the vertex attribute and set a default null value
     */
    public void disable() {
        GL20.glDisableVertexAttribArray(location);
        switch (attributeSize) {
        case 1:
            GL20.glVertexAttrib1f(location, 0f);
            break;
        case 2:
            GL20.glVertexAttrib2f(location, 0f, 0f);
            break;
        case 3:
            GL20.glVertexAttrib3f(location, 0f, 0f, 1f);
            break;
        case 4:
            GL20.glVertexAttrib4f(location, 0f, 0f, 1f, 0);
            break;
        default:
            throw new IllegalStateException("unknowns size: '" + typeSize + "'");
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [shaderProgramId=" + shaderProgramId 
                + ", name=" + name 
                + ", location=" + location + "]";
    }

    // TODO: move this into a subclass and reuse for uniforms
    private final void setupTypeInfo(int typeId, int typeSize) {
        switch (typeId) {
        case GL11.GL_FLOAT:
            type = "GL_FLOAT";
            attributeSize = 1;
            break;
        case GL20.GL_FLOAT_VEC2:
            type = "GL_FLOAT_VEC2";
            attributeSize = 2;
            break;
        case GL20.GL_FLOAT_VEC3:
            type = "GL_FLOAT_VEC3";
            attributeSize = 3;
            break;
        case GL20.GL_FLOAT_VEC4:
            type = "GL_FLOAT_VEC4";
            attributeSize = 4;
            break;
        case GL20.GL_FLOAT_MAT2:
            type = "GL_FLOAT_MAT2";
            attributeSize = 2;
            break;
        case GL20.GL_FLOAT_MAT3: 
            type = "GL_FLOAT_MAT3";
            attributeSize = 3;
            break;
        case GL20.GL_FLOAT_MAT4: 
            type = "GL_FLOAT_MAT4";
            attributeSize = 4;
            break;
        case GL21.GL_FLOAT_MAT2x3: 
            type = "GL_FLOAT_MAT2x3";
            attributeSize = 6;
            break;
        case GL21.GL_FLOAT_MAT2x4: 
            type = "GL_FLOAT_MAT2x4";
            attributeSize = 8;
            break;
        case GL21.GL_FLOAT_MAT3x2: 
            type = "GL_FLOAT_MAT3x2";
            attributeSize = 6;
            break;
        case GL21.GL_FLOAT_MAT3x4: 
            type = "GL_FLOAT_MAT3x4";
            attributeSize = 12;
            break;
        case GL21.GL_FLOAT_MAT4x2: 
            type = "GL_FLOAT_MAT4x2";
            attributeSize = 8;
            break;
        case GL21.GL_FLOAT_MAT4x3: 
            type = "GL_FLOAT_MAT4x3";
            attributeSize = 12;
            break;
        case GL11.GL_INT:
            type = "GL_INT";
            attributeSize = 1;
            break;
        case GL20.GL_INT_VEC2:
            type = "GL_INT_VEC2";
            attributeSize = 2;
            break;
        case GL20.GL_INT_VEC3: 
            type = "GL_INT_VEC3";
            attributeSize = 3;
            break;
        case GL20.GL_INT_VEC4: 
            type = "GL_INT_VEC4";
            attributeSize = 4;
            break;
        case GL11.GL_UNSIGNED_INT: 
            type = "GL_UNSIGNED_INT";
            attributeSize = 1;
            break;
        case GL30.GL_UNSIGNED_INT_VEC2: 
            type = "GL_UNSIGNED_INT_VEC2";
            attributeSize = 2;
            break;
        case GL30.GL_UNSIGNED_INT_VEC3: 
            type = "GL_UNSIGNED_INT_VEC3";
            attributeSize = 3;
            break;
        case GL30.GL_UNSIGNED_INT_VEC4: 
            type = "GL_UNSIGNED_INT_VEC4";
            attributeSize = 4;
            break;
        case GL11.GL_DOUBLE:
            type = "GL_DOUBLE";
            attributeSize = 2;
            break;
        case GL40.GL_DOUBLE_VEC2: 
            type = "GL_DOUBLE_VEC2";
            attributeSize = 4;
            break;
        case GL40.GL_DOUBLE_VEC3: 
            type = "GL_DOUBLE_VEC3";
            attributeSize = 6;
            break;
        case GL40.GL_DOUBLE_VEC4: 
            type = "GL_DOUBLE_VEC4";
            attributeSize = 8;
            break;
        case GL40.GL_DOUBLE_MAT2: 
            type = "GL_DOUBLE_MAT2";
            attributeSize = 4;
            break;
        case GL40.GL_DOUBLE_MAT3: 
            type = "GL_DOUBLE_MAT3";
            attributeSize = 6;
            break;
        case GL40.GL_DOUBLE_MAT4: 
            type = "GL_DOUBLE_MAT4";
            attributeSize = 8;
            break;
        case GL40.GL_DOUBLE_MAT2x3: 
            type = "GL_DOUBLE_MAT2x3";
            attributeSize = 12;
            break;
        case GL40.GL_DOUBLE_MAT2x4: 
            type = "GL_DOUBLE_MAT2x4";
            attributeSize = 16;
            break;
        case GL40.GL_DOUBLE_MAT3x2: 
            type = "GL_DOUBLE_MAT3x2";
            attributeSize = 12;
            break;
        case GL40.GL_DOUBLE_MAT3x4: 
            type = "GL_DOUBLE_MAT3x4";
            attributeSize = 24;
            break;
        case GL40.GL_DOUBLE_MAT4x2: 
            type = "GL_DOUBLE_MAT4x2";
            attributeSize = 16;
            break;
        case GL40.GL_DOUBLE_MAT4x3:
            type = "GL_DOUBLE_MAT4x3";
            attributeSize = 24;
            break;
        default:
            type = "unknown";
            attributeSize = 0;
            break;
        }
    }

}
