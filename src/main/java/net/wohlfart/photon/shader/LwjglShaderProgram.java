package net.wohlfart.photon.shader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import net.wohlfart.photon.resources.ResourceTool;
import net.wohlfart.photon.scene.IGeometry.VertexFormat;
import net.wohlfart.photon.shader.ShaderUniformHandle.ShaderUniformValue;

import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.glu.GLU;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;

/**
 * DefaultShaderProgram class for lwjgl
 * 
 * see: https://github.com/mattdesl/lwjgl-basics/blob/master/src/mdesl/graphics/
 * glutils/ShaderProgram.java
 */
public class LwjglShaderProgram implements IShaderProgram {

    private static final int BYTES_PER_FLOAT = 4;

    protected static final Logger LOGGER = LoggerFactory.getLogger(LwjglShaderProgram.class);
    
    public static final IShaderProgram NULL_SHADER = new IShaderProgram.NullShader();


    private final URI vertexShader;
    private final URI fragmentShader;

    private int vertexShaderId = -1;
    private int fragmentShaderId = -1;

    private int programId = -1;

    private Map<String, ShaderUniformHandle> uniforms = new HashMap<>();
    private Map<String, ShaderAttributeHandle> attributes = new HashMap<>();

    LwjglShaderProgram(IShaderProgramIdentifier identifier) {
        this.vertexShader = identifier.getVertexShaderResource();
        this.fragmentShader = identifier.getFragmentShaderResource();
    }

    // delayed since the OpenGL context needs to be up in order for this to work
    @Override
    public void setup() {
        vertexShaderId = loadShader(vertexShader, GL20.GL_VERTEX_SHADER);
        fragmentShaderId = loadShader(fragmentShader, GL20.GL_FRAGMENT_SHADER);
        linkAndValidate(vertexShaderId, fragmentShaderId);

        findUniforms();
        findAttributes();
    }

    @Override
    public void bind() {
        if (programId == -1) {
            setup();
        }
        LOGGER.debug("binding programId '{}' ", programId);
        GL20.glUseProgram(programId);
    }

    @Override
    public void unbind() {
        GL20.glUseProgram(0);
    }

    @Override
    public void dispose() {
        unlink(vertexShaderId, fragmentShaderId);
        GL20.glDeleteProgram(programId);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [vertexShaderId=" + vertexShaderId 
                + ", fragmentShaderId=" + fragmentShaderId 
                + ", programId=" + programId + "]";
    }

    @Override
    public void setupAttributes(VertexFormat vertexFormat) {
        int attributeSize;
        int stride = vertexFormat.getTotalSize() * BYTES_PER_FLOAT;
        int offset = 0;

        attributeSize = vertexFormat.positionSize();
        setupAttribute(ShaderTemplateWrapper.VERTEX_POSITION, attributeSize, stride, offset);
        offset += (attributeSize * BYTES_PER_FLOAT);

        attributeSize = vertexFormat.colorSize();
        setupAttribute(ShaderTemplateWrapper.VERTEX_COLOR, attributeSize, stride, offset);
        offset += (attributeSize * BYTES_PER_FLOAT);

        attributeSize = vertexFormat.normalSize();
        setupAttribute(ShaderTemplateWrapper.VERTEX_NORMAL, attributeSize, stride, offset);
        offset += (attributeSize * BYTES_PER_FLOAT);

        attributeSize = vertexFormat.textureSize();
        setupAttribute(ShaderTemplateWrapper.VERTEX_TEXTURE, attributeSize, stride, offset);
        // offset += (attributeSize * BYTES_PER_FLOAT);     
    }

    @Override
    public void setUniform(String uniformName, int textureSlot) {
        ShaderUniformHandle currentHandle = getUniformHandle(uniformName);
        if (currentHandle == null) {
            LOGGER.error("uniform for texture slot '{}' can't be found in shader {}, skipping this texture", uniformName, this);
            return;
        }       
        currentHandle.setTextureIndex(textureSlot);
    }

    @Override
    public void updateShadersUniforms(Set<String> textureNames, Map<String, ShaderUniformValue> uniformValues) {
        for (String uniformName : getUniformHandleNames()) {
            // check if the uniform slot is a defined texture, if so we can skip that since
            // we already did set the texture index on that uniform
            if (!textureNames.contains(uniformName)) {
                ShaderUniformValue uniformValue = uniformValues.get(uniformName);
                setUniform(uniformName, uniformValue);
            }
        }    
    }


    protected void unlink(int... handles) {
        GL20.glUseProgram(0);
        for (final int handle : handles) {
            GL20.glDetachShader(programId, handle);
        }
        for (final int handle : handles) {
            GL20.glDeleteShader(handle);
        }
    }
   
    private ShaderUniformHandle getUniformHandle(String string) {
        return uniforms.get(string);
    };

    private Set<String> getUniformHandleNames() {
        return uniforms.keySet();
    }

    private ShaderAttributeHandle getVertexAttributeHandle(String string) {
        return attributes.get(string);
    }

    private Set<String> getVertexAttributeHandleNames() {
        return attributes.keySet();
    }

    private void setUniform(String uniformName, ShaderUniformValue uniformValue) {
        ShaderUniformHandle currentHandle = getUniformHandle(uniformName);
        if (currentHandle == null) {
            LOGGER.error("uniform for '{}' can't be found in shader {}, skipping this uniform", uniformName, this);
            return;
        }

        if (uniformValue == null) {
            LOGGER.error("uniform or texture value for '{}' can't be found", uniformName);
            return;
        }

        uniformValue.accept(currentHandle);
    }

    private int loadShader(final URI uri, int shaderType) {
        LOGGER.info("loading shader from '{}' type is '{}'", uri, shaderType);
        int shader = 0;

        try (final InputStream inputStream = ResourceTool.openStream(uri)) { 

            final String code = readShaderCode(inputStream);                         
            shader = ARBShaderObjects.glCreateShaderObjectARB(shaderType);
            if (shader == 0) {
                throw new ShaderException("glCreateShaderObjectARB returned 0");
            }

            LOGGER.debug("shader code: " + code);
            ARBShaderObjects.glShaderSourceARB(shader, code);
            ARBShaderObjects.glCompileShaderARB(shader);

            final int compileStatus = ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB);
            if (compileStatus == GL11.GL_FALSE) {
                throw new ShaderException("Error creating shader, couldn't compile, " 
                        + " reason: " + getLogInfo(shader) 
                        + " the shader resource is '" + uri + "'," 
                        + " the shaderType is '" + shaderType + "'");
            }            
            return shader;
        } catch (final FileNotFoundException ex) {
            throw new ShaderException("file not found: '" + uri + "'", ex);
        } catch (final NullPointerException ex) {
            throw new ShaderException("null pointer, file not found: '" + uri + "'", ex);
        } catch (final IOException ex) {
            throw new ShaderException("stream problems", ex);
        }
    }

    private String readShaderCode(InputStream inputStream) throws IOException {
        String string = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));
        ShaderTemplateWrapper stringTemplate = new ShaderTemplateWrapper(string);
        return stringTemplate.render();
    }

    /**
     * attach, link and validate the shaders into a shader program
     * 
     * @param handles
     *            the shader handles
     */
    private void linkAndValidate(int... handles) {
        int error = GL11.glGetError();
        if (error != GL11.GL_NO_ERROR) {// @formatter:off
            throw new ShaderException("" + "error before linking shader, error string is '" + GLU.gluErrorString(error) + "' \n" + "programmId is '"
                    + programId + "' \n" + "handles are: " + Arrays.toString(handles)); // @formatter:on
        }        
        programId = GL20.glCreateProgram();
        for (final int handle : handles) {
            GL20.glAttachShader(programId, handle);
        }
        GL20.glLinkProgram(programId);
        GL20.glValidateProgram(programId);
        error = GL11.glGetError();
        if (error != GL11.GL_NO_ERROR) {// @formatter:off
            throw new ShaderException("" + "error validating shader, error string is '" + GLU.gluErrorString(error) + "' \n" + "programmId is '"
                    + programId + "' \n" + "handles are: " + Arrays.toString(handles)); // @formatter:on
        }
    }

    private void findUniforms() {
        int len = GL20.glGetProgrami(programId, GL20.GL_ACTIVE_UNIFORMS);
        int strLen = GL20.glGetProgrami(programId, GL20.GL_ACTIVE_UNIFORM_MAX_LENGTH);

        for (int i = 0; i < len; i++) {
            String name = GL20.glGetActiveUniform(programId, i, strLen);
            int location = GL20.glGetUniformLocation(programId, name);
            ShaderUniformHandle handle = new ShaderUniformHandle(programId, name, location);
            uniforms.put(name, handle);
            LOGGER.info("created uniform handle: " + handle);
        }
    }

    private void findAttributes() {
        int len = GL20.glGetProgrami(programId, GL20.GL_ACTIVE_ATTRIBUTES);
        int strLen = GL20.glGetProgrami(programId, GL20.GL_ACTIVE_ATTRIBUTE_MAX_LENGTH);

        for (int i = 0; i < len; i++) {
            String name = GL20.glGetActiveAttrib(programId, i, strLen);
            int size = GL20.glGetActiveAttribSize(programId, i);
            int type = GL20.glGetActiveAttribType(programId, i);
            int location = GL20.glGetAttribLocation(programId, name);
            ShaderAttributeHandle handle = new ShaderAttributeHandle(this.programId, name, size, type, location);
            attributes.put(name, handle);
            LOGGER.info("created attribute handle: " + handle);
        }
    }

    private String getLogInfo(int obj) {
        return ARBShaderObjects.glGetInfoLogARB(obj, ARBShaderObjects.glGetObjectParameteriARB(obj, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB));
    }

    // TODO: compare shader attribute sizes with size from handler
    private void setupAttribute(String attributeName, int attributeSize, int stride, int offset) {
        ShaderAttributeHandle currentHandle = getVertexAttributeHandle(attributeName);
        if (attributeSize > 0) {
            if (currentHandle != null)  {    
                if (attributeSize == currentHandle.getAttributeSize()) {
                    currentHandle.enable(attributeSize, stride, offset);
                } else {
                    LOGGER.info("attribute '{}' has different size in shader '{}' ({} != {})", 
                            new Object[] {attributeName, this, attributeSize, currentHandle.getAttributeSize()});
                }
            } else {
                LOGGER.info("attribute '{}' not found in shader '{}', available attribute names are {}",
                        attributeName, this, getVertexAttributeHandleNames());
            }
        } else {
            if (currentHandle != null)  { 
                currentHandle.disable();
            }
        }
    }

}
