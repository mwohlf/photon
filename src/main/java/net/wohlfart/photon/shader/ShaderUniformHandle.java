package net.wohlfart.photon.shader;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;

public class ShaderUniformHandle {

 
    public static final ShaderUniformValue SHADER_UNIFORM_NULL_VALUE = new ShaderUniformNullValue();
    
    
    private final int shaderProgramId;
    
    private final String name;
    private final int location;
    
    
    public interface ShaderUniformValue {      
        
        void accept(ShaderUniformHandle handle); 
    }
    
    
    public static class ShaderUniformNullValue implements ShaderUniformValue {
        @Override
        public void accept(ShaderUniformHandle handle) {
            // do nothing
        }   
    }
       
    public static class ShaderUniformMatrixValue implements ShaderUniformValue {
        private final Matrix4f matrix;
        
        public ShaderUniformMatrixValue(Matrix4f matrix) {
            this.matrix = matrix;
        }
        
        public void set(Matrix4f matrix) {
            this.matrix.load(matrix);
        }
             
        public Matrix4f get() {
            return matrix;
        }
  
        @Override
        public void accept(ShaderUniformHandle handle) {
            handle.set(matrix);
        }
        
    }
    
    public static class ShaderTextureIndexValue implements ShaderUniformValue {
        private final int index;
        
        ShaderTextureIndexValue(int index) {
            this.index = index;
        }
        
        @Override
        public void accept(ShaderUniformHandle handle) {
            handle.setTextureIndex(index);
        }
       
    }
    

    public ShaderUniformHandle(int shaderProgramId, String name, int location) {
        if (location < 0) {
            throw new IllegalArgumentException("uniform: '" + name + "' has location '" + location + "'");
        }
        this.shaderProgramId = shaderProgramId;
        this.name = name;
        this.location = location;
    }

    // this is to avoid gc when creating a new buffer for each call
    private final static ThreadLocal<FloatBuffer> matrix4Buffer = new ThreadLocal<FloatBuffer>() {{ set(BufferUtils.createFloatBuffer(16)); }};
    public void set(Matrix4f matrix) {
        assert (matrix != null) : "Uniform '" + name + "' is empty";
        matrix.store(matrix4Buffer.get());
        GL20.glUniformMatrix4(location, false, (FloatBuffer)matrix4Buffer.get().flip()); // false: no transpose
    }
    
    public void setTextureIndex(int index) {
        GL20.glUniform1i(location, index);
    }
    
    
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [shaderProgramId=" + shaderProgramId 
                + ", name=" + name 
                + ", location=" + location + "]";
    }


}
