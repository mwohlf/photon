package net.wohlfart.photon.hud;

/**
 * steps to create a layouted layer...
 * 
 * - instantiate a container with a dimension and a layout manager
 * - add elements to the container
 * - use the returned constraints to modfy the elements position
 * 
 * ------ internal structure
 * 
 *  ContainerLayer: implements IEntity3D and is considered the
 *                  "semantic" node in the scene graph for
 *                  the elements
 *             
 *  Elements: they implement the RenderCommands for the semantic view
 *            they know their 
 *  
 *  Container: has Elements and a LayoutManager, also is a RenderCommand      
 *         
 *  LayoutManager: property of a container
 *  
 */

