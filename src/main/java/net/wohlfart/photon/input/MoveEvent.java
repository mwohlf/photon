package net.wohlfart.photon.input;

import javax.annotation.Nullable;

import net.wohlfart.photon.tools.ObjectPool;
import net.wohlfart.photon.tools.ObjectPool.PoolableObject;
import net.wohlfart.photon.tools.OutOfResourcesException;

import org.lwjgl.util.vector.Vector3f;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MoveEvent extends Vector3f implements PoolableObject {
    private static final long serialVersionUID = 1L;
    protected static final Logger LOGGER = LoggerFactory.getLogger(MoveEvent.class);
    protected static final int POOL_SIZE = 100; 
    private static final float MOVE_SPEED = 50f;
    private static final float WHEEL_SENSITIVITY = 0.05f;

    private static final ObjectPool<MoveEvent> POOL = new ObjectPool<MoveEvent>(POOL_SIZE) {
        @Override
        protected MoveEvent newObject() {
            return new MoveEvent();
        }
    };

    @Override
    public void reset() {
        x = y = z = 0;
        POOL.returnObject(this);
    }

    // only the pool may create an instance
    private MoveEvent() {}

    // ---- package private static factory methods

    static MoveEvent wheel(float time, int delta) {
        return move(0, 0, time * MOVE_SPEED * WHEEL_SENSITIVITY * delta);
    }

    static MoveEvent moveRight(float time) {
        return move(+time * MOVE_SPEED, 0, 0);
    }

    static MoveEvent moveLeft(float time) {
        return move(-time * MOVE_SPEED, 0, 0);
    }

    static MoveEvent moveDown(float time) {
        return move(0, -time * MOVE_SPEED, 0);
    }

    static MoveEvent moveUp(float time) {
        return move(0, +time * MOVE_SPEED, 0);
    }

    static MoveEvent moveForward(float time) {
        return move(0, 0, -time * MOVE_SPEED);
    }

    static MoveEvent moveBack(float time) {
        return move(0, 0, +time * MOVE_SPEED);
    }

    // package private for testing
    @Nullable static MoveEvent move(float x, float y, float z) {
        try {
            final MoveEvent result = POOL.borrowObject();
            result.x = x;
            result.y = y;
            result.z = z;
            return result;
        } catch (OutOfResourcesException ex) {
            LOGGER.warn("returning null", ex);
            return null;
        }
    }

}
