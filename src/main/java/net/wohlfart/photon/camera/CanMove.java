package net.wohlfart.photon.camera;

import org.lwjgl.util.vector.Vector3f;

public interface CanMove {

    public void move(Vector3f vector);
    
    public void reset();

    public Vector3f getPosition();

    public void setPosition(Vector3f vector);

}
