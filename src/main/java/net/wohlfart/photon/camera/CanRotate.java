package net.wohlfart.photon.camera;

import org.lwjgl.util.vector.Quaternion;
import org.lwjgl.util.vector.Vector3f;

public interface CanRotate {

    public void rotate(float deltaAngle, Vector3f axis);
    
    public void reset();

    public Quaternion getRotation();

    public void setRotation(Quaternion quaternion);

    public Vector3f getRght(Vector3f vector3f);

    public Vector3f getUp(Vector3f result);

    public Vector3f getForward(Vector3f result);

}
