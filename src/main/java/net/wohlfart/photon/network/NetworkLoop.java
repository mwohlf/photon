package net.wohlfart.photon.network;

import net.wohlfart.photon.AbstractTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NetworkLoop extends AbstractTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(NetworkLoop.class);
    private static final String NAME = NetworkLoop.class.getSimpleName();
    
    public NetworkLoop() {
        super(NAME);
        this.thread.setDaemon(true);
    }

    @Override
    public void run() {
        LOGGER.info("starting run method");
    }

    public void startupPlatform() {
        // nothing yet
    }

    public void processEvents(float delta) {
        // nothing yet     
    }

}
