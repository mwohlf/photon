package net.wohlfart.photon.texture;

public class NullTexture implements ITexture {
    
    public static final ITexture INSTANCE = new NullTexture();

    private NullTexture() {
        // do nothing        
    }
    
    @Override
    public int getHandle() {
        return 0;
    }

}
