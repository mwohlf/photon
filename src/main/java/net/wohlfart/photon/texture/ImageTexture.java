package net.wohlfart.photon.texture;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;


// see: http://www.java-gaming.org/index.php?topic=25516.0
public class ImageTexture implements ITexture {
    private static int BYTES_PER_PIXEL = 4;

    protected int textureId = -1;

    protected final ByteBuffer buffer;
    protected final int width;
    protected final int height;
   
    public ImageTexture(BufferedImage image) {
        width = image.getWidth();
        height = image.getHeight();
        
        int[] pixels = new int[width * height];
        image.getRGB(0, 0, width, height, pixels, 0, width);

        buffer = BufferUtils.createByteBuffer(width * height * BYTES_PER_PIXEL); //4 for RGBA, 3 for RGB
        
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                int pixel = pixels[y * width + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF));     // red component
                buffer.put((byte) ((pixel >> 8) & 0xFF));      // green component
                buffer.put((byte) (pixel & 0xFF));             // blue component
                buffer.put((byte) ((pixel >> 24) & 0xFF));     // alpha component for RGBA
            }
        }
        buffer.flip();
    }

    protected int getWidth() {
        return width;
    }

    protected int getHeight() {
        return height;
    }

    public void setup() {  
        // see: http://lwjgl.org/wiki/index.php?title=The_Quad_textured
        textureId = GL11.glGenTextures();     
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);
     
        // setup the ST coordinate system
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
        // setup what to do when the texture has to be scaled
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);

        GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);

        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);
        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
    }

    @Override
    public int getHandle() {
        if (textureId == -1) {
            setup();
        }
        return textureId;
    }

}
