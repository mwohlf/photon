package net.wohlfart.photon.texture;

import java.net.URI;

public interface ITexture {
    
    public interface ITextureIdentifier {
 
        URI getTextureResource();
                
    }
  
    int getHandle();

}
