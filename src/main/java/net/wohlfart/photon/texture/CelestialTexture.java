package net.wohlfart.photon.texture;

import java.awt.Color;
import java.nio.IntBuffer;
import java.util.Random;

import net.wohlfart.photon.tools.MathTool;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector3f;


// see: http://www.java-gaming.org/index.php?topic=25516.0
public class CelestialTexture implements ITexture {
    
    private final Random random;
    
    protected int textureId = -1;
    
    protected final IntBuffer intBuffer;
    protected final int width;
    protected final int height;


    public CelestialTexture(float radius, CelestialType type, long seed /*, int textureUnit*/) {
        this((int) (radius * 2f * (float) Math.PI + 0.5f),
             (int) (radius * 2f * (float) Math.PI + 0.5f),
             type,
             seed);
    }

    CelestialTexture(int width, int height, CelestialType celestialType, long seed /*, int textureUnit*/) {
        this.width = width;
        this.height = height;
        this.random = new Random(seed);

        intBuffer = BufferUtils.createIntBuffer(width * height);

        // random for texture variation
        final float textureVariant = random.nextFloat();

        final int[] data = new int[width * height]; // 4 byte
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                final Vector3f vector = getNormalVector(x, y);
                Color color = celestialType.getColor(vector.x, vector.y, vector.z, textureVariant);
                setPixel(x, y, color, width, height, data);
            }
        }
        intBuffer.put(data);
        intBuffer.flip();
        intBuffer.rewind();
    }
    
    @Override
    public int getHandle() {
        if (textureId == -1) {
            setup();
        }
        return textureId;
    }

    public void setup() {
        // Create a new texture object in memory and bind it
        textureId = GL11.glGenTextures();   // generate texture ID
      //  GL13.glActiveTexture(textureUnit);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureId);   // bind texture ID

        // all RGB bytes are aligned to each other and each component is 1 byte
        GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);
        // upload the texture data and generate mip maps (for scaling)
        GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, width, height, 0, GL11.GL_RGBA, GL12.GL_UNSIGNED_INT_8_8_8_8_REV, intBuffer);
        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);

        //Setup wrap mode for the ST coordinate system
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT); // GL_CLAMP
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT); // GL_CLAMP

        // Setup what to do when the texture has to be scaled
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR);

        // All RGB bytes are aligned to each other and each component is 1 byte
        //GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);
        // GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, width, height, 0, GL12.GL_BGRA, GL12.GL_UNSIGNED_INT_8_8_8_8_REV, texture);
        //GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, width, height, 0, GL12.GL_BGRA, GL12.GL_UNSIGNED_INT_8_8_8_8_REV, buffer);
        //GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, intBuffer);
    }

    /**
     * this does a 2D to 3D transformation
     * 0/0 is top left, the whole texture is wrapped around the celestial object
     *
     * @return a vector with each element [0..1]
     */
    private final Vector3f getNormalVector(int x, int y) {
        final int yRange = height - 1;
        final int xRange = width - 1;

        return MathTool.getNormalVector((float)x/(float)xRange, (float)y/(float)yRange);
    }

    private void setPixel(int x, int y, Color color, int width, int height, int[] data) {
        y = height - y - 1;
        if (x < 0) {
            x = 0;
        }
        if (y < 0) {
            y = 0;
        }
        if (x > width - 1) {
            x = width - 1;
        }
        if (y > height - 1) {
            y = height - 1;
        }

        final int i = x + y * width;
        int value = 0;
        value = value | 0xff & color.getAlpha();
        value = value << 8;
        value = value | 0xff & color.getBlue();
        value = value << 8;
        value = value | 0xff & color.getGreen();
        value = value << 8;
        value = value | 0xff & color.getRed();
        data[i] = value;
    }

}
