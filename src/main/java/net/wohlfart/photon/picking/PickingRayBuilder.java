package net.wohlfart.photon.picking;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

public class PickingRayBuilder {

    private float x;
    private float y;
    private float width;
    private float height;
    private Matrix4f projectionMatrix;
    private Matrix4f modelViewMatrix;

   
    public PickingRayBuilder setX(float x) {
        this.x = x;
        return this;
    }

    public PickingRayBuilder setY(float y) {
        this.y = y;
        return this;
    }

    public PickingRayBuilder setWidth(float width) {
        this.width = width;
        return this;
    }

    public PickingRayBuilder setHeight(float height) {
        this.height = height;
        return this;
    }

    public PickingRayBuilder setProjectionMatrix(Matrix4f projectionMatrix) {
        this.projectionMatrix = projectionMatrix;
        return this;
    }

    public PickingRayBuilder setModelViewMatrix(Matrix4f modelViewMatrix) {
        this.modelViewMatrix = modelViewMatrix;
        return this;
    }

    





    public PickingRay build() {

        Matrix4f matrx = new Matrix4f();

        Matrix4f.mul(projectionMatrix, modelViewMatrix, matrx);
        matrx = Matrix4f.invert(matrx, matrx);

        final Vector4f cameraSpaceNear = new Vector4f(x / width * 2f - 1f, y / height * 2f - 1f, -1.0f, 1.0f);
        final Vector4f cameraSpaceFar = new Vector4f(x / width * 2f - 1f, y / height * 2f - 1f, 1.0f, 1.0f);

        final Vector4f worldSpaceNear = new Vector4f();
        Matrix4f.transform(matrx, cameraSpaceNear, worldSpaceNear);

        final Vector4f worldSpaceFar = new Vector4f();
        Matrix4f.transform(matrx, cameraSpaceFar, worldSpaceFar);

        // @formatter:off
        final Vector3f start = new Vector3f(
                worldSpaceNear.x / worldSpaceNear.w,
                worldSpaceNear.y / worldSpaceNear.w,
                worldSpaceNear.z / worldSpaceNear.w);
        final Vector3f end = new Vector3f(
                worldSpaceFar.x / worldSpaceFar.w,
                worldSpaceFar.y / worldSpaceFar.w,
                worldSpaceFar.z / worldSpaceFar.w);
        // @formatter:on

        return new PickingRay(start, end);
    }
}
