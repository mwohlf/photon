package net.wohlfart.photon;

import net.wohlfart.photon.resources.ResourceManager;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * to lookup resources that can't be injected at boot-up time,
 * be careful if you use this class in a constructor since the 
 * application context might not yet be available
 */
public final class ContextHolder implements ApplicationContextAware {
    
    private static final String RESOURCE_MANAGER_ID = "resourceManager";
    private static final String SETTINGS_ID = "gameSettings";

    private static volatile ApplicationContext applicationContext;
    private static volatile ResourceManager resourceManager;

    private static Object lock = new Object();
    
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        synchronized (lock) {
            if (ContextHolder.applicationContext != null) {
                throw new IllegalStateException("applicationContext alread set");
            } else {
                ContextHolder.applicationContext = applicationContext;                
            }
        }
    }

    /**
     * simplifies loading a resource
     */
    public static <P,K> P loadResource(Class<P> clazz, K key) {
        if (resourceManager == null) {
            resourceManager = getResourceManager();
        }
        return resourceManager.load(clazz, key);
    }
    
    private static ResourceManager getResourceManager() {
        assert (applicationContext != null);
        return applicationContext.getBean(RESOURCE_MANAGER_ID, ResourceManager.class);
    }

    public static Settings getSettings() {
        assert (applicationContext != null);
        return applicationContext.getBean(SETTINGS_ID, Settings.class);    
    }
    
}
