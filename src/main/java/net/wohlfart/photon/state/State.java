package net.wohlfart.photon.state;

import net.wohlfart.photon.api.Disposable;
import net.wohlfart.photon.api.Renderable;
import net.wohlfart.photon.api.Updateable;

public interface State extends Disposable, Updateable, Renderable {

    boolean isDone();

    public Event getTransitionEvent();

    public void setup();

}
