package net.wohlfart.photon.state;


public class DefaultState implements State {
    
    public static final State INIT_STATE = new DefaultState() {
        @Override
        public Event getTransitionEvent() {
            return Event.START;
        }
    };

    public static final State END_STATE = new DefaultState();


    @Override
    public void dispose() {
    }

    @Override
    public boolean isDone() {
        return true;
    }

    @Override
    public void update(float delta) {
    }

    @Override
    public void render() {
    }

    @Override
    public Event getTransitionEvent() {
        return Event.END;
    }

    @Override
    public void setup() {
    }

}
