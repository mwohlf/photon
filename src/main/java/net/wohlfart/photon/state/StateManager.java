package net.wohlfart.photon.state;

public class StateManager {

    private State simpleState;
    @SuppressWarnings("unused")
    private State defaultState;

    public State calculateNextState(State current) {
        Event transitionEvent = current.getTransitionEvent();
        if (transitionEvent == Event.START) {
            return simpleState;
        } 
        return DefaultState.END_STATE;
    }

    public void setSimpleState(State simpleState) {
        this.simpleState = simpleState;
    }

    public void setDefaultState(State defaultState) {
        this.defaultState = defaultState;
    }

}
