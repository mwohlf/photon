package net.wohlfart.photon.renderer;

import static net.wohlfart.photon.ContextHolder.loadResource;

import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.graph.Tree;
import net.wohlfart.photon.shader.IShaderProgram;
import net.wohlfart.photon.shader.IShaderProgram.IShaderProgramIdentifier;
import net.wohlfart.photon.shader.LwjglShaderProgram;
import net.wohlfart.photon.shader.ShaderUniformHandle.ShaderUniformValue;
import net.wohlfart.photon.texture.ITexture;
import net.wohlfart.photon.tools.Dimension;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementing OpenGl and lwjgl specific stuff here
 */
public class LwjglRenderer implements Renderer {

    protected static final Logger LOGGER = LoggerFactory.getLogger(LwjglRenderer.class);
   
    private static final int[] TEXTURE_SLOTS = new int[] {
        GL13.GL_TEXTURE0, GL13.GL_TEXTURE1, GL13.GL_TEXTURE2, GL13.GL_TEXTURE3, GL13.GL_TEXTURE4, GL13.GL_TEXTURE5,
        GL13.GL_TEXTURE6, GL13.GL_TEXTURE7, GL13.GL_TEXTURE8, GL13.GL_TEXTURE9, GL13.GL_TEXTURE10, GL13.GL_TEXTURE11};


    // also contains the texture indices
    private HashMap<String, ShaderUniformValue> uniformValues = new HashMap<String, ShaderUniformValue>();

    private Dimension screenDimension;

    private IShaderProgram currentShader = LwjglShaderProgram.NULL_SHADER;

    private LwjglRenderConfig currentRenderConfig = LwjglRenderConfig.NULL_CONFIG;

    private volatile boolean debug;
    

    @Override
    public Dimension getScreenDimension() {
        return screenDimension;
    }

    @Override
    public void setScreenDimension(Dimension screenDimension) {
        this.screenDimension = screenDimension;
    }

    @Override
    public void setDebugMode(boolean debug) {
        this.debug = debug;
    }

    @Override
    public void renderParent(Tree<IRenderNode> tree) {
        final IRenderNode node = tree.getValue();
        
        if (debug) { LOGGER.error("rendering: {}", node); }
        node.accept(this, tree);    
       
    }
    
    @Override
    public void renderChildren(Tree<IRenderNode> tree) {
        final Iterator<? extends Tree<IRenderNode>> iter = tree.getChildren();
        while (iter.hasNext()) {
            if (debug) { LOGGER.error(" {"); }
            renderParent(iter.next());
            if (debug) { LOGGER.error(" }"); }
        }
    }
    
    

    @Override
    public void setConfig(IShaderProgramIdentifier shaderId, IRenderConfig<LwjglRenderConfig> nextRenderConfig) {
        
        //IShaderProgramIdentifier shaderId = command.getShaderId();        
        currentShader = loadResource(IShaderProgram.class, shaderId);
        currentShader.bind();

        // IRenderConfig<LwjglRenderConfig> nextRenderConfig = command.getRenderConfig();
        currentRenderConfig = nextRenderConfig.pushDiffs(currentRenderConfig);
    }
    
    @Override
    public void setUniformValues(Map<String, ITexture> textures, Map<String, ShaderUniformValue> uniforms) {

        // see: http://stackoverflow.com/questions/16497794/sending-two-textures-to-glsl-shader
        //Map<String, ITexture> textures = command.getTextures();
        int slot = 0;
        for (Map.Entry<String, ITexture> entry : textures.entrySet()) {
            final String key = entry.getKey();
            final ITexture texture = entry.getValue();

            GL13.glActiveTexture(TEXTURE_SLOTS[slot]);
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.getHandle());
            currentShader.setUniform(key, slot); 
            slot++;
            if (slot> TEXTURE_SLOTS.length) {
                break;
            }
        }
    
        //uniformValues.putAll(command.getUniformValues()); // this also contains the textures...
        uniformValues.putAll(uniforms); // this also contains the textures...
        currentShader.updateShadersUniforms(textures.keySet(), uniformValues); // ...which we try to exclude when setting uniforms only
    }
        
     @Override
    public void drawGeometry(IGeometry geometry) {

        //final IGeometry geometry = command.getGeometry();  
        int vaoHandle = geometry.getHandle();

        if (vaoHandle != -1) {
            GL30.glBindVertexArray(vaoHandle);            
        } else {
            // encapsulates all the data that is associated with the vertex processor
            vaoHandle = GL30.glGenVertexArrays();
            GL30.glBindVertexArray(vaoHandle);
            geometry.setHandle(vaoHandle);

            createAndBindVboHandle(geometry);
            currentShader.setupAttributes(geometry.getVertexFormat());

            if (geometry.isIndexed()) {
                // render with an index buffer           
                LwjglBufferIndexFormat.createAndBindIdxBufferHandle(geometry);
            }
        }

        final int primitiveType = getPrimitiveType(geometry.getStreamFormat());
        if (geometry.isIndexed()) {        
            GL11.glDrawElements( // see: http://www.opengl.org/wiki/GlDrawElements
                    primitiveType, // mode: primitive type see: http://www.opengl.org/wiki/Primitive
                    geometry.getIndicesCount(), // indicesCount
                    LwjglBufferIndexFormat.getIndexElemSize(geometry), // indexElemSize
                    0); // indexOffset
        } else {
            // render plain vertices without indices
            GL11.glDrawArrays(primitiveType, // mode: primitive type see: http://www.opengl.org/wiki/Primitive
                    0, geometry.getVerticesCount());
        }

        // unbind
        GL30.glBindVertexArray(0);
    }
     
    // keep the OpenGL stuff inside this class
    private int getPrimitiveType(Geometry.StreamFormat streamFormat) {
        switch (streamFormat) {
        case LINES:
            return GL11.GL_LINES;
        case LINE_STRIP:
            return GL11.GL_LINE_STRIP;
        case LINE_LOOP:
            return GL11.GL_LINE_LOOP;
        case TRIANGLES:
            return GL11.GL_TRIANGLES;
        default:
            throw new IllegalArgumentException("unknown stream format: " + streamFormat);
        }
    }

    private int createAndBindVboHandle(IGeometry geometry) {
        final FloatBuffer verticesBuffer = geometry.createVertexFloatBuffer();
        final int vboVerticesHandle = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboVerticesHandle);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, verticesBuffer, GL15.GL_STATIC_DRAW);
        return vboVerticesHandle;
    }

}
