package net.wohlfart.photon.renderer;

public interface IRenderConfig<T extends IRenderConfig<T>> {
    
    public T pushDiffs(T oldState);
    
    public boolean isTranslucent();

}
