package net.wohlfart.photon.renderer;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import net.wohlfart.photon.scene.IGeometry;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;

// methods for handling the index array depending on the required size
// FIXME: check negative overflow
public enum LwjglBufferIndexFormat {
    INT {
        @Override
        int doGetIndexElemSize() {
            return GL11.GL_UNSIGNED_INT;
        };

        @Override
        int doCreateAndBindIdxBufferHandle(IGeometry geometry) {
            final IntBuffer indicesBuffer = geometry.createIndexIntBuffer();
            final int idxBufferHandle = GL15.glGenBuffers();
            GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, idxBufferHandle);
            GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL15.GL_STATIC_DRAW);
            return idxBufferHandle;
        }
    },
    SHORT {
        @Override
        int doGetIndexElemSize() {
            return GL11.GL_UNSIGNED_SHORT;
        };

        @Override
        int doCreateAndBindIdxBufferHandle(IGeometry geometry) {
            final ShortBuffer indicesBuffer = geometry.createIndexShortBuffer();
            final int idxBufferHandle = GL15.glGenBuffers();
            GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, idxBufferHandle);
            GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL15.GL_STATIC_DRAW);
            return idxBufferHandle;
        }
    },
    BYTE {
        @Override
        int doGetIndexElemSize() {
            return GL11.GL_UNSIGNED_BYTE;
        };

        @Override
        int doCreateAndBindIdxBufferHandle(IGeometry geometry) {
            final ByteBuffer indicesBuffer = geometry.createIndexByteBuffer();
            final int idxBufferHandle = GL15.glGenBuffers();
            GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, idxBufferHandle);
            GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL15.GL_STATIC_DRAW);
            return idxBufferHandle;
        }
    };

    abstract int doGetIndexElemSize();

    abstract int doCreateAndBindIdxBufferHandle(IGeometry geometry);

    // return the minimum size for the index buffer, needed for a defined count of indices
    private static LwjglBufferIndexFormat get(int indicesCount) {
        if (indicesCount > Short.MAX_VALUE) {
            return INT;
        } else if (indicesCount > Byte.MAX_VALUE) {
            return SHORT;
        } else {
            return BYTE;
        }
    }
    
    static int createAndBindIdxBufferHandle(IGeometry geometry) {
        return get(geometry.getIndicesCount()).doCreateAndBindIdxBufferHandle(geometry);
    }

    static int getIndexElemSize(IGeometry geometry) {
        return get(geometry.getIndicesCount()).doGetIndexElemSize();
    }

}