package net.wohlfart.photon.renderer;

import java.util.Map;

import org.lwjgl.util.vector.Matrix4f;

import net.wohlfart.photon.scene.IGeometry;
import net.wohlfart.photon.scene.graph.NodeSortStrategy.HasSortToken;
import net.wohlfart.photon.scene.graph.Tree;
import net.wohlfart.photon.shader.IShaderProgram.IShaderProgramIdentifier;
import net.wohlfart.photon.shader.ShaderUniformHandle.ShaderUniformValue;
import net.wohlfart.photon.texture.ITexture;
import net.wohlfart.photon.tools.Dimension;

public interface Renderer {
       
    public interface IRenderNode extends HasSortToken {

        // the render calls, must call renderer.renderChildren(tree)
        void accept(Renderer renderer, Tree<IRenderNode> tree);
                         
    }
    
    
    /* implementations of this interface contain all data needed to 
     * rendered an element on screen by a renderer and also
     * everything to be sorted into the render queue/cache
     * 
     * 
     * see: http://stackoverflow.com/questions/16497794/sending-two-textures-to-glsl-shader
     * for multi texture binding
     */
    public interface IRenderElem extends IRenderNode {
         
        // the shader uniforms
        Map<String, ShaderUniformValue> getUniformValues();

        // texture uniforms
        Map<String, ITexture> getTextures();       

        // contains the vertex attributes, in some cases we return subclasses of IGeometry
        IGeometry getGeometry();

        // the matrix that transforms this element form model to world space
        Matrix4f getModel2WorldMatrix();

        void setZOrder(double zOrder);
            
    }

    
    public interface IRenderEffect extends IRenderNode {

        FrameBufferObject getFrameBufferObject();
        
    }


    // init call to start rendering the command cache
    void renderParent(Tree<IRenderNode> tree);
    
    void renderChildren(Tree<IRenderNode> tree);

    void setDebugMode(boolean enableDebug);

    
    
    // FIXME: remove these methods

    Dimension getScreenDimension();
    
    void setScreenDimension(Dimension screenDimension);



    // call sequence for drawing a  standard render command from the render cache
    void setConfig(IShaderProgramIdentifier shaderId, IRenderConfig<LwjglRenderConfig> nextRenderConfig);

    void setUniformValues(Map<String, ITexture> textures, Map<String, ShaderUniformValue> uniformValues);

    void drawGeometry(IGeometry geometry);



}
