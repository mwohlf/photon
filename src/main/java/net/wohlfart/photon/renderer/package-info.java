package net.wohlfart.photon.renderer;


/*
 
 
 Render Cache
 ------------
 
 
 HasSortToken
     ^
     |
    IRenderNode
     ^        ^
     |        |
 IRenderElem  IRenderEffect
 
 
 
 Semantic elements
 -----------------
 
 AbstractEntity3D
      ^
      |
   IEntity3D  
      ^
      |
      
  
 
 
 */
/**
 * lets try and keep platform stuff in this package, for each platform there should be
 * implementations of the Renderer interface
 *
 *
 * Renderer and Scene nodes implement the visitor pattern with double dispatch:
 * 
 * - input for the renderer is the scene graph which is organized in the Scene class
 * - during the first render pass the renderer calls the accept method on a node
 * - the node calls one of the visit methods in the renderer
 * - the visit method does whatever is needed to perform the rendering
 *    * setting shader
 *    * setting uniforms (cam/view matrix)
 *    *  
 * 
 * 
 
 concepts/ideas:
 
 
 Element3D: a real world element or semantic node in the scene graph
            it consists of one or more RenderUnits
 
 RenderUnit: a renderable object which contains all the resources needed for
             a single call to drawElements/drawArrays
             
             has exactly one RenderState
             has exactly one Shader
             migh have one or more Textures
             has one associated VAO including VBOs
             
 RenderState: element contains a render units properties for rendering and
              is used for sorting the RenderUnits             
 
 
 
 rendering should be done:
 
  - clear
  - opaque object front to back, 
  - then skypbox with special renderer http://www.gamedev.net/topic/577973-skybox-and-depth-buffer/
  - transparent objects back to front
  - effects
  - hud (maybe earlier)
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 */

