package net.wohlfart.photon.renderer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL31;


/**
 *  see: "3D Engine Design for Virtual Globes"
 */
public class LwjglRenderConfig implements IRenderConfig<LwjglRenderConfig> {

    // need to setup the weights frst since they are used in the constructor...
    static final EnumWeights weights = new EnumWeights(
            Blending.class,   // opaque objects first front to back, then the transparent back to front
            DepthTest.class,
            // probably never change:
            ColorMask.class,
            SissorTest.class,
            StencilTest.class,
            // values should never change:
            ClearDepth.class,
            ClearColor.class,
            FaceCulling.class,
            PrimitiveRestartIndex.class);

    public static final LwjglRenderConfig DEFAULT = new LwjglRenderConfig(
            Blending.OFF,
            ClearColor.GREY,
            ClearDepth.ONE,
            ColorMask.ON,
            DepthTest.GL_LEQUAL,
            FaceCulling.OFF,
            PrimitiveRestartIndex.MAX_INT,
            SissorTest.OFF,
            StencilTest.OFF);

    public static final LwjglRenderConfig BLENDING_ON = new LwjglRenderConfig(
            Blending.ON,
            ClearColor.GREY,
            ClearDepth.ONE,
            ColorMask.ON,
            DepthTest.GL_LEQUAL,
            FaceCulling.OFF,
            PrimitiveRestartIndex.MAX_INT,
            SissorTest.OFF,
            StencilTest.OFF);

    public static final LwjglRenderConfig DEFAULT_3D = new LwjglRenderConfig(
            Blending.OFF,
            ClearColor.GREY,
            ClearDepth.ONE,
            ColorMask.ON,
            DepthTest.GL_LEQUAL,                  //  
            FaceCulling.BACK,                   // use BACK for production
            PrimitiveRestartIndex.MAX_INT,
            SissorTest.OFF,
            StencilTest.OFF);

    public static final LwjglRenderConfig SKYBOX = new LwjglRenderConfig(
            Blending.OFF,
            ClearColor.GREY,
            ClearDepth.ONE,
            ColorMask.ON,
            DepthTest.GL_LEQUAL,                  
            FaceCulling.BACK,                   
            PrimitiveRestartIndex.MAX_INT,
            SissorTest.OFF,
            StencilTest.OFF);

    public static final LwjglRenderConfig NULL_CONFIG = new LwjglRenderConfig();




    private final Blending blending;

    private final ClearColor clearColor;

    private final ClearDepth clearDepth;

    private final ColorMask colorMask;

    private final DepthTest depthTest;

    private final FaceCulling faceCulling;

    private final PrimitiveRestartIndex primitiveRestartIndex;

    private final SissorTest scissorTest;

    private final StencilTest stencilTest;



    private final int hash;


    LwjglRenderConfig() {
        this.blending = null;
        this.clearColor = null;
        this.clearDepth = null;
        this.colorMask = null;
        this.depthTest = null;
        this.faceCulling = null;
        this.primitiveRestartIndex = null;
        this.scissorTest = null;
        this.stencilTest = null; 
        hash = Integer.MIN_VALUE;
    }

    LwjglRenderConfig(
            Blending blending,
            ClearColor clearColor,
            ClearDepth clearDepth,
            ColorMask colorMask,
            DepthTest depthTest,
            FaceCulling faceCulling,
            PrimitiveRestartIndex primitiveRestartIndex,
            SissorTest scissorTest,
            StencilTest stencilTest) {

        this.blending = blending;
        this.clearColor = clearColor;
        this.clearDepth = clearDepth;
        this.colorMask = colorMask;
        this.depthTest = depthTest;
        this.faceCulling = faceCulling;
        this.primitiveRestartIndex = primitiveRestartIndex;
        this.scissorTest = scissorTest;
        this.stencilTest = stencilTest; 

        hash = weights.getWeightFor(blending,
                clearColor,
                clearDepth,
                colorMask,
                depthTest,
                faceCulling,
                primitiveRestartIndex,
                scissorTest,
                stencilTest); 
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if ((object == null) || (this.getClass() != object.getClass())) {
            return false;
        }
        LwjglRenderConfig that = (LwjglRenderConfig) object;
        return this.hash == that.hash;
    }

    @Override
    public LwjglRenderConfig pushDiffs(LwjglRenderConfig oldState) {
        if (oldState.hash == this.hash) {
            return this;
        }

        blending.pushIfDiff(oldState.blending);
        clearColor.pushIfDiff(oldState.clearColor);
        clearDepth.pushIfDiff(oldState.clearDepth);
        colorMask.pushIfDiff(oldState.colorMask);
        depthTest.pushIfDiff(oldState.depthTest);      
        faceCulling.pushIfDiff(oldState.faceCulling);
        primitiveRestartIndex.pushIfDiff(oldState.primitiveRestartIndex);
        scissorTest.pushIfDiff(oldState.scissorTest);
        stencilTest.pushIfDiff(oldState.stencilTest);
        return this;
    }
    

    @Override
    public boolean isTranslucent() {
        return blending == Blending.ON;
    }

    

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [blending=" + blending 
                + ", clearColor=" + clearColor 
                + ", clearDepth=" + clearDepth 
                + ", colorMask=" + colorMask
                + ", depthTest=" + depthTest 
                + ", hash=" + hash + "]";
    }


    
    interface PropertyPusher<P extends PropertyPusher<P>> {
        // implementations get called with the old property value
        void pushIfDiff(P property);
    } 


    public enum Blending implements PropertyPusher<Blending> {
        OFF {
            @Override
            public void pushIfDiff(Blending blending) {
                if (blending != OFF) {
                    GL11.glDisable(GL11.GL_BLEND);
                }
            }
        },
        ON {
            @Override
            public void pushIfDiff(Blending blending) {
                if (blending != ON) {
                    GL11.glEnable(GL11.GL_BLEND);
                    GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
                }
            }
        }
    }

    public enum ClearColor implements PropertyPusher<ClearColor> {
        BLACK {
            @Override
            public void pushIfDiff(ClearColor clearColor) {
                if (clearColor != BLACK) {
                    GL11.glClearColor(0f, 0f, 0f, 0.5f);
                }
            }
        },
        GREY {
            @Override
            public void pushIfDiff(ClearColor clearColor) {
                if (clearColor != GREY) {
                    GL11.glClearColor(0.5f, 0.5f, 0.5f, 0.5f);
                }
            }
        },
        WHITE {
            @Override
            public void pushIfDiff(ClearColor clearColor) {
                if (clearColor != WHITE) {
                    GL11.glClearColor(1f, 1f, 1f, 0.5f);
                }
            }
        }
    }

    public enum ClearDepth implements PropertyPusher<ClearDepth> {
        ONE {
            @Override
            public void pushIfDiff(ClearDepth clearDepth) {
                if (clearDepth != ONE) {
                    GL11.glClearDepth(1f);
                }
            }
        }
    }

    public enum ColorMask implements PropertyPusher<ColorMask> {
        ON {
            @Override
            public void pushIfDiff(ColorMask colorMask) {
                if (colorMask != ON) {
                    GL11.glColorMask(true, true, true, true);  
                }
            }
        }
    }

    public enum DepthTest implements PropertyPusher<DepthTest> {
        GL_LEQUAL {
            @Override
            public void pushIfDiff(DepthTest depthTest) {
                if (depthTest != GL_LEQUAL) {
                    GL11.glEnable(GL11.GL_DEPTH_TEST);
                    GL11.glDepthFunc(GL11.GL_LEQUAL);           
                }
            }                 
        },
        GL_LESS {
            @Override
            public void pushIfDiff(DepthTest depthTest) {
                if (depthTest != GL_LESS) {
                    GL11.glEnable(GL11.GL_DEPTH_TEST);
                    GL11.glDepthFunc(GL11.GL_LESS);           
                }
            }                 
        },
        OFF {
            @Override
            public void pushIfDiff(DepthTest depthTest) {
                if (depthTest != OFF) {
                    GL11.glDisable(GL11.GL_DEPTH_TEST);
                }
            }
        };              
    }

    public enum FaceCulling implements PropertyPusher<FaceCulling> {
        BACK { // the default
            @Override
            public void pushIfDiff(FaceCulling faceCulling) {
                if (faceCulling != BACK) {
                    GL11.glEnable(GL11.GL_CULL_FACE);
                    GL11.glCullFace(GL11.GL_BACK);
                }
            }            
        },
        FRONT {
            @Override
            public void pushIfDiff(FaceCulling faceCulling) {
                if (faceCulling != FRONT) {
                    GL11.glEnable(GL11.GL_CULL_FACE);
                    GL11.glCullFace(GL11.GL_FRONT);
                }
            }                 
        },
        FRONT_AND_BACK {
            @Override
            public void pushIfDiff(FaceCulling faceCulling) {
                if (faceCulling != FRONT_AND_BACK) {
                    GL11.glEnable(GL11.GL_CULL_FACE);
                    GL11.glCullFace(GL11.GL_FRONT_AND_BACK);
                }
            }   
        },
        OFF {
            @Override
            public void pushIfDiff(FaceCulling faceCulling) {
                if (faceCulling != OFF) {
                    GL11.glDisable(GL11.GL_CULL_FACE);
                }
            }
        };          
    }

    public enum PrimitiveRestartIndex implements PropertyPusher<PrimitiveRestartIndex> {
        MAX_INT {
            @Override
            public void pushIfDiff(PrimitiveRestartIndex primitiveRestartIndex) {
                if (primitiveRestartIndex != MAX_INT) {
                    GL31.glPrimitiveRestartIndex(Integer.MAX_VALUE);
                }
            }
        }
    }

    public enum SissorTest implements PropertyPusher<SissorTest> {
        OFF {
            @Override
            public void pushIfDiff(SissorTest sissorTest) {
                if (sissorTest != OFF) {
                    GL11.glDisable(GL11.GL_DEPTH_TEST);
                }
            }
        }
    }

    public enum StencilTest implements PropertyPusher<StencilTest> {
        OFF {
            @Override
            public void pushIfDiff(StencilTest stencilTest) {
                if (stencilTest != OFF) {
                    GL11.glDisable(GL11.GL_DEPTH_TEST);
                }
            }
        }
    }

}
