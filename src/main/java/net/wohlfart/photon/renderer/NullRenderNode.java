package net.wohlfart.photon.renderer;

import net.wohlfart.photon.renderer.Renderer.IRenderNode;
import net.wohlfart.photon.scene.graph.NodeSortStrategy;
import net.wohlfart.photon.scene.graph.Tree;
import net.wohlfart.photon.scene.graph.NodeSortStrategy.ISortToken;

public class NullRenderNode implements IRenderNode {
    
    protected final String name;
    
    public NullRenderNode(String name) {
        this.name = name;
    }

    @Override
    public void accept(Renderer renderer, Tree<IRenderNode> tree) {
        // do nothing
    }

    @Override
    public ISortToken getSortToken() {
        return NodeSortStrategy.NULL_SORT_TOKEN;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " [name=" + name + "]";
    }

}