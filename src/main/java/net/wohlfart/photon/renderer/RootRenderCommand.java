package net.wohlfart.photon.renderer;

import net.wohlfart.photon.renderer.Renderer.IRenderNode;
import net.wohlfart.photon.scene.graph.Tree;

import org.lwjgl.opengl.GL11;

public class RootRenderCommand extends NullRenderNode {

    public RootRenderCommand(String name) {
        super(name);
    }

    @Override
    public void accept(Renderer renderer, Tree<IRenderNode> tree) {
        clear();
        renderer.renderChildren(tree);
    }
    
    private void clear() {
        GL11.glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
        GL11.glClearDepth(1f);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glDepthFunc(GL11.GL_LEQUAL);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT | GL11.GL_STENCIL_BUFFER_BIT);          
    }
    
}
