package net.wohlfart.photon;

/**
 * a collection of application settings that can be configured at startup.
 */
public final class Settings {

    private String title = "lwjgl";

    private int width = 800;
    private int height = 600;

    private int sync = 100;

    private float fieldOfView = 60f;
    private float nearPlane = 0.1f;
    private float farPlane = 100f;

    private boolean fullscreen = false;
    private boolean vSyncEnabled = true;

    private int pixelPerCentimeter = 50;
    private float fontScale = 1;

    private boolean useThreads = true;

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public float getFieldOfView() {
        return fieldOfView;
    }

    public void setFieldOfView(float fieldOfView) {
        this.fieldOfView = fieldOfView;
    }

    public float getNearPlane() {
        return nearPlane;
    }

    public void setNearPlane(float nearPlane) {
        this.nearPlane = nearPlane;
    }

    public boolean getFullscreen() {
        return fullscreen;
    }

    public void setFullscreen(boolean fullscreen) {
        this.fullscreen = fullscreen;
    }

    public float getFarPlane() {
        return farPlane;
    }

    public void setFarPlane(float farPlane) {
        this.farPlane = farPlane;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean getVSyncEnabled() {
        return vSyncEnabled;
    }

    public void setVSyncEnabled(boolean vSyncEnabled) {
        this.vSyncEnabled = vSyncEnabled;
    }

    public int getPixelPerCentimeter() {
        return pixelPerCentimeter;
    }

    public void setPixelPerCentimeter(int pixelPerCentimeter) {
        this.pixelPerCentimeter = pixelPerCentimeter;
    }

    public float getFontScale() {
        return fontScale;
    }

    public void setFontScale(float fontScale) {
        this.fontScale = fontScale;
    }

    public boolean isUseThreads() {
        return useThreads;
    }

    public void setUseThreads(boolean useThreads) {
        this.useThreads = useThreads;
    }

}
