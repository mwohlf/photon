package net.wohlfart.photon.time;

import net.wohlfart.photon.api.Disposable;

/**
 * A generic Timer interface, a timer is based on a clock.
 */
public interface Timer extends Disposable {

    /**
     * Returns the time in seconds since the last call this this method.
     * 
     * @return a float with the time in seconds since the last call to
     *         getDelta()
     */
    float getDelta();

    /**
     * Destroy the timer and free its resources.
     */
    @Override
    void dispose();

}
