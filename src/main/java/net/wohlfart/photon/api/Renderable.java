package net.wohlfart.photon.api;

public interface Renderable {

    public void render();

}
