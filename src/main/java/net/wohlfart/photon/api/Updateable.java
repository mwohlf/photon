package net.wohlfart.photon.api;

public interface Updateable {

    public void update(float delta);

}
