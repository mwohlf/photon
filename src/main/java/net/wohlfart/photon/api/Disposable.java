package net.wohlfart.photon.api;

public interface Disposable {

    public void dispose();

}
