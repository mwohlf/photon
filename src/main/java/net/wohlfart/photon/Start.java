package net.wohlfart.photon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Start {
    private static final Logger LOGGER = LoggerFactory.getLogger(Start.class);

    private static final String CONFIG_FILE = "config/applicationContext.xml";

    public static void main(String[] args) {
        // see: http://docs.oracle.com/javase/7/docs/api/java/lang/System.html#getProperties()
        bootInfo();
        try (final ConfigurableApplicationContext appContext = new ClassPathXmlApplicationContext(CONFIG_FILE)) {
            Application game = appContext.getBean(Application.class);
            game.start();
        } catch (Exception ex) {
            LOGGER.error("exception in main thread", ex);
        }
    }

    private static void bootInfo() {
        bootInfo(
                "java.version", 
                "java.vendor", 
                "java.vm.version",   
                "java.vm.vendor",  
                "java.vm.name"                
                );
    }

    private static void bootInfo(String... strings) {
        for (String string : strings) {
            LOGGER.info(string + ": " + System.getProperty(string));
        }    
    }

}
