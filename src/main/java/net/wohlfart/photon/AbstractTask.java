package net.wohlfart.photon;

import net.wohlfart.photon.input.CommandEvent;
import net.wohlfart.photon.tools.Subscribe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * a base class for running an application task (gfx, input, network)
 */
public abstract class AbstractTask implements Runnable {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTask.class);

    protected final Thread thread;
    protected volatile boolean isTerminated;
    

    protected AbstractTask(String threadName) {
        this.thread = new Thread(this, threadName);
        this.isTerminated = false;
    }

    public final void start() {
        thread.start();
    }

    @Subscribe
    public void terminateThreads(CommandEvent event) {
        LOGGER.debug("incoming command: " + event);
        switch (event.getKey()) {
            case EXIT:
                isTerminated = true;
                break;
            default:
                LOGGER.debug("unknown command: " + event + ", ignoring");
        }
    }

    public void join() {
        try {
            thread.join();
        } catch (InterruptedException ex) {
            throw new IllegalStateException("join failed", ex);
        }
    }

}
