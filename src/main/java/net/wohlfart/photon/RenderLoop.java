package net.wohlfart.photon;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.wohlfart.photon.renderer.Renderer;
import net.wohlfart.photon.shader.ShaderTemplateWrapper;
import net.wohlfart.photon.shader.ShaderUniformHandle.ShaderUniformMatrixValue;
import net.wohlfart.photon.shader.ShaderUniformHandle.ShaderUniformValue;
import net.wohlfart.photon.state.DefaultState;
import net.wohlfart.photon.state.State;
import net.wohlfart.photon.state.StateManager;
import net.wohlfart.photon.texture.ITexture;
import net.wohlfart.photon.time.Clock;
import net.wohlfart.photon.time.Timer;
import net.wohlfart.photon.time.TimerImpl;
import net.wohlfart.photon.tools.Dimension;
import net.wohlfart.photon.tools.EventBus;
import net.wohlfart.photon.tools.MathTool;
import net.wohlfart.photon.tools.ObjectPool.PoolableObject;
import net.wohlfart.photon.tools.PerspectiveProjectionFab;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.EXTFramebufferMultisample;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.vector.Matrix4f;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * this class does the rendering of the current state and also 
 * implements the state switching
 */
public class RenderLoop extends AbstractTask  {
    private static final Logger LOGGER = LoggerFactory.getLogger(RenderLoop.class);
    private static final String NAME = RenderLoop.class.getSimpleName();

    private Clock clock;
    private Settings settings;
    private StateManager stateManager;

    private Renderer renderer;

    protected State currentState;

    /* we need to remember the initial display mode so we can reset it on exit */
    private DisplayMode origDisplayMode;

    private EventBus<PoolableObject> eventBus;


    public RenderLoop() {
        super(NAME);
        this.thread.setDaemon(false);
    }

    public void setClock(Clock clock) {
        this.clock = clock;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public void setStateManager(StateManager stateManager) {
        this.stateManager = stateManager;
    }

    public void setRenderer(Renderer renderer) {
        this.renderer = renderer;
    }

    public void setEventBus(EventBus<PoolableObject> eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public void run() {
        try {
            // side effect during startup is fixing the settings' height/width value
            // and initializing the OpenGL environment
            startupPlatform();
            renderStateLoop();
            shutdownPlatform();
        } catch (final GenericException ex) {
            LOGGER.warn("Application startup failed", ex);
        }
    }

    void startupPlatform() throws GenericException {
        if (settings.getFullscreen()) {
            setupFullscreen();
        } else {
            setupWindow();
        }
        // if nothing exploded so far we have a valid OpenGL context
        // map the internal OpenGL coordinate system to the entire viewport
        GL11.glViewport(0, 0, settings.getWidth(), settings.getHeight());
        GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_NICEST);
        GL11.glEnable(GL20.GL_VERTEX_PROGRAM_POINT_SIZE); // not sure what this is supposed to do

        // default values
        GL11.glFrontFace(GL11.GL_CCW);

        final Dimension screenDimension = new Dimension(settings.getWidth(), settings.getHeight());
        renderer.setScreenDimension(screenDimension); 

        Map<String, ShaderUniformValue> uniforms = new HashMap<>();
        // our cam is fixed at 0/0/0
        uniforms.put(ShaderTemplateWrapper.UNIFORM_WORLD_2_CAM_MTX, new ShaderUniformMatrixValue(MathTool.IDENT_MATRIX));
        // cam field of view, no need to ever change that
        final Matrix4f cameraToClipMatrix = PerspectiveProjectionFab.create(settings);
        uniforms.put(ShaderTemplateWrapper.UNIFORM_CAM_2_CLIP_MTX, new ShaderUniformMatrixValue(cameraToClipMatrix));

        renderer.setUniformValues(Collections.<String, ITexture>emptyMap(), uniforms);

        // some debug output
        LOGGER.info("Vendor: " + GL11.glGetString(GL11.GL_VENDOR));
        LOGGER.info("Version: " + GL11.glGetString(GL11.GL_VERSION));
        LOGGER.info("max. Vertex Attributes: " + GL11.glGetInteger(GL20.GL_MAX_VERTEX_ATTRIBS));
        LOGGER.info("max. Texture Image Units: " + GL11.glGetInteger(GL20.GL_MAX_TEXTURE_IMAGE_UNITS));
        LOGGER.info("GL_EXT_framebuffer_object: " + GLContext.getCapabilities().GL_EXT_framebuffer_object);
        LOGGER.info("GL_EXT_packed_depth_stencil: " + GLContext.getCapabilities().GL_EXT_packed_depth_stencil);
        LOGGER.info("GL_EXT_framebuffer_multisample: " + GLContext.getCapabilities().GL_EXT_framebuffer_multisample);
        LOGGER.info("GL_EXT_framebuffer_blit: " + GLContext.getCapabilities().GL_EXT_framebuffer_blit);
        LOGGER.info("GL_MAX_SAMPLES_EXT: " + GL11.glGetInteger(EXTFramebufferMultisample.GL_MAX_SAMPLES_EXT));     

    }

    private void renderStateLoop() {
        final Timer globalTimer = new TimerImpl(clock);

        State nextState = DefaultState.INIT_STATE;
        while (nextState != DefaultState.END_STATE && isTerminated == false) {
            switchToState(nextState);
            while (!nextState.isDone() && !isTerminated) {
                updateAndRenderState(globalTimer.getDelta());
            }
            nextState = stateManager.calculateNextState(nextState);
        }

        globalTimer.dispose();
    }

    private void shutdownPlatform() {
        if (origDisplayMode != null) {
            try {
                Display.setDisplayMode(origDisplayMode);
            } catch (final LWJGLException ex) {
                LOGGER.warn("error while shutting down", ex);
            }
        }
        Display.destroy();
    }

    // state switching
    protected void switchToState(State newState) {
        if (currentState != null) {
            eventBus.unregister(currentState);
            currentState.dispose();
        }
        currentState = newState;
        eventBus.register(currentState);
        currentState.setup();
    }

    protected void updateAndRenderState(float delta) {
        LOGGER.debug("[ms]/frame: {} ; frame/[s]: {}", delta, 1f / delta);
        // call the models to do their things
        currentState.update(delta);
        // do the render magic
        currentState.render();
        Display.sync(settings.getSync());
        // draw the (double-)buffer to the screen, don't read user input
        // input is read with processMessages in a different thread
        Display.update(false);
        // do some work
        //while (eventBus.hasEvent()) {
        if (eventBus.hasEvent()) {
            eventBus.fireEvent();
        }
        eventBus.flush();
    }

    private void setupWindow() throws GenericException {
        try {
            // see: http://lwjgl.org/forum/index.php/topic,2951.0.html
            // for more about setting up a display...
            final PixelFormat pixelFormat = new PixelFormat();
            final ContextAttribs contextAtributes = new ContextAttribs(3, 3).withForwardCompatible(true).withProfileCore(true);
            Display.setDisplayMode(new DisplayMode(settings.getWidth(), settings.getHeight()));
            Display.setResizable(false);
            Display.setTitle(settings.getTitle());
            Display.create(pixelFormat, contextAtributes); // creates the GL context
        } catch (LWJGLException ex) {
            throw new GenericException("unable to setup a window", ex);
        }
    }

    private void setupFullscreen() throws GenericException {
        DisplayMode[] modes;
        try {
            modes = Display.getAvailableDisplayModes();
        } catch (LWJGLException ex) {
            throw new GenericException("can't figure out the available resolutions", ex);
        }
        DisplayMode requestedResolution = null;
        DisplayMode bestResolution = null;
        for (final DisplayMode mode : modes) {
            if (bestResolution == null || mode.getWidth() > bestResolution.getWidth() || mode.getHeight() > bestResolution.getHeight()) {
                bestResolution = mode;
            }
            if (mode.getWidth() == settings.getWidth() && mode.getHeight() == settings.getHeight()) {
                requestedResolution = mode;
            }
        }
        if (bestResolution == null) {
            throw new GenericException("can't figure out the best resolution");
        }
        origDisplayMode = Display.getDisplayMode();
        final PixelFormat pixelFormat = new PixelFormat();
        final ContextAttribs contextAtributes = new ContextAttribs(3, 3).withForwardCompatible(true).withProfileCore(true);

        try {
            Display.setDisplayMode(bestResolution);
            Display.setFullscreen(true);
            Display.setVSyncEnabled(settings.getVSyncEnabled());
            if (requestedResolution != null) {
                Display.setDisplayMode(requestedResolution);
            } else {
                Display.setDisplayMode(bestResolution);
                final int width = bestResolution.getWidth();
                final int height = bestResolution.getHeight();
                // not sure if this is a good idea since other clients might already
                // use the old values..
                LOGGER.warn("fixing width/height to: {}/{}", width, height);
                settings.setWidth(width);
                settings.setHeight(height);
            }
            Display.create(pixelFormat, contextAtributes); // creates the GL context
        } catch (LWJGLException ex) {
            throw new GenericException("can't figure out the available resolutions", ex);
        }
    }

}
